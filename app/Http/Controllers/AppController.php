<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Models\File;
use App\Models\Pelatihan;
use App\Models\Pengaduan;
use App\Models\Refleksi;
use App\Models\RefleksiTemp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class AppController extends Controller
{
    public function refleksi()
    {
        return view('refleksi');
    }
    public function pengaduan()
    {
        return view('pengaduan');
    }
    public function pengaduan_store(Request $r)
    {
        $pengaduan = new Pengaduan();
        $pengaduan->nama = $r->nama;
        if ($r->tgllahir != null) {
            $pengaduan->tgllahir = \Carbon\Carbon::createFromFormat('d-m-Y', $r->tgllahir);
        }
        if ($r->tanggal != null) {
            $pengaduan->tanggal = \Carbon\Carbon::createFromFormat('d-m-Y', $r->tanggal);
        }
        $pengaduan->jk = $r->jk;
        $pengaduan->nohp = $r->nohp;
        $pengaduan->email = $r->email;
        $pengaduan->pekerjaan = $r->pekerjaan;
        $pengaduan->instansi = $r->instansi;
        $pengaduan->uraian = $r->uraian;
        $pengaduan->alamat = $r->alamat;
        $pengaduan->status = "Pengaduan Baru";
        $pengaduan->jenis = $r->jenis;
        $pengaduan->save();
        if ($r->hasFile('bukti')) {
            foreach ($r->file('bukti') as $file) {
                $originalName = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $fileName = Str::slug(pathinfo($originalName, PATHINFO_FILENAME)) . '_' . time() . '.' . $extension;

                // Simpan file ke storage dengan nama unik
                $file->move(public_path('upload'), $fileName);

                // Simpan path ke database
                $path = 'upload/' . $fileName;
                $bukti = new File([
                    'id_pengaduan' => $pengaduan->id,
                    'nama_file' => $originalName,
                    'path' => $path
                ]);
                $bukti->save();
            }
        }
        if ($pengaduan->jenis == "P") {
            return redirect("/hasil-pengaduan/$pengaduan->token");
        } else {
            return redirect("/hasil-pengaduan-wbs/$pengaduan->token");
        }
    }
    public function kirim_pengaduan($id)
    {
        $data = Pengaduan::where('token', $id)->first();
        return view('kirim', compact('data'));
    }
    public function cek_status(Request $r)
    {
        $data = Pengaduan::where('token', $r->tiket)->first();
        if ($data->jenis == "P") {
            return redirect("/hasil-pengaduan/$data->token");
        } elseif ($data->jenis == "W") {
            return redirect("/hasil-pengaduan-wbs/$data->token");
        } elseif ($data->jenis == "R") {
            Alert::error('Tiket Tidak Ditemukan', 'Coba Periksa Kembali Tiket Anda');
            return back();
        } else {
            Alert::error('Tiket Tidak Ditemukan', 'Coba Periksa Kembali Tiket Anda');
            return back();
        }
    }
    public function wbs()
    {
        return view('wbs');
    }
    public function cek_token_pelatihan(Request $r)
    {
        $data = Pelatihan::where('token', $r->token)->first();
        if ($data) {
            $refleksi = Refleksi::where('id_pelatihan', $data->id)->get();
            $res = ['status' => 'ada', 'data' => $data, 'refleksi' => $refleksi];
        } else {
            $res = ['status' => 'tidak'];
        }
        return response()->json($res);
    }
    public function tambah_refleksi_temp(Request $r)
    {
        $refleksiTemp = new RefleksiTemp();
        $refleksiTemp->uraian = $r->uraian;
        $refleksiTemp->id_pelatihan = $r->id_tambah;
        $refleksiTemp->save();

        return response()->json(['pesan' => 'sukses', 'id' => $refleksiTemp->id_pelatihan]);
    }
    public function pengaduan_refleksi($id)
    {
        $itung = RefleksiTemp::where('id_pelatihan', $id)->count();
        if ($itung > 0) {
            $data
                = RefleksiTemp::where('id_pelatihan', $id)->get();
            $res = ['status' => 'ada', 'data' => $data];
        } else {
            $res = ['status' => 'tidak'];
        }
        return response()->json($res);
    }
    public function hapus_refleksi_temp(Request $r)
    {
        $data
            = RefleksiTemp::where('id', $r->id)->delete();
        return response()->json(['pesan' => 'sukses']);
    }
    public function tambah_refleksi(Request $r)
    {
        $cek = Refleksi::where('id_pelatihan', $r->id_tambah)
            ->where('tanggal', Carbon::createFromFormat('d-m-Y', $r->tanggal)->toDateString())->count();
        if ($cek > 0) {
            $res = ['status' => 'sudah'];
        } else {

            $itung = RefleksiTemp::where('id_pelatihan', $r->id_tambah)->count();
            if ($itung > 0) {
                $refleksi = new Refleksi;
                $refleksi->id_pelatihan = $r->id_tambah;
                $refleksi->tanggal = Carbon::createFromFormat('d-m-Y', $r->tanggal);
                $refleksi->save();
                $ref
                    = RefleksiTemp::where('id_pelatihan', $r->id_tambah)->get();
                foreach ($ref as $isi) {
                    $pel = Pelatihan::where('id', $r->id_tambah)->first();
                    $data = new Pengaduan;
                    $data->id_refleksi = $refleksi->id;
                    $data->jenis = "R";
                    $data->nama_pelatihan = $pel->nama_pelatihan;
                    $data->mot = $pel->mot;
                    $data->uraian = $isi->uraian;
                    $data->status = "Pengaduan Baru";
                    $data->save();
                    RefleksiTemp::where('id', $isi->id)->delete();
                }

                $pelatihan = Pelatihan::where('id', $r->id_tambah)->first();
                $res = ['status' => 'sukses', 'data' => $pelatihan];
            } else {
                $res = ['status' => 'kosong'];
            }
        }
        return response()->json($res);
    }
    public function detail_refleksi_temp($id)
    {
        $refleksi = Refleksi::where('id', $id)->first();
        $pelatihan = Pengaduan::where('id_refleksi', $id)->get();
        $isi = [];

        foreach ($pelatihan as $data) {
            if ($data->status_selesai) {
                $status = $data->status_selesai;
                $keterangan = $data->ket_selesai;
            } elseif ($data->status_bagian) {
                $status = $data->status_bagian;
                $keterangan = $data->ket_bagian;
            } elseif ($data->status_pemberian_bagian) {
                $status = $data->status_pemberian_bagian;
                $keterangan = "Diterima Oleh Bagian" . " " . $data->bagian;
            } elseif ($data->status_ka) {
                $status = $data->status_ka;
                $keterangan = $data->respon_ka;
            } elseif ($data->status == "Diverifikasi Verifikator") {
                $status = $data->status;
                $keterangan = $data->respon_verifikasi;
            } elseif ($data->status == "Ditolak Verifikator") {
                $status = $data->status;
                $keterangan = $data->respon_verifikasi;
            } else {
                $status = $data->status;
                $keterangan = "Sedang Menunggu Verifikasi";
            }

            // Menambahkan data ke dalam larik $isi
            $isi[] = ['id' => $data->id, 'uraian' => $data->uraian, 'status' => $status, 'keterangan' => $keterangan];
        }

        $tanggal = Carbon::parse($refleksi->tanggal)->format('d-m-Y');
        $res = ['tanggal' => $tanggal, 'data' => $isi];

        return response()->json($res);
    }
}
