<?php

namespace App\Http\Controllers;

use App\Models\Bukti;
use App\Models\File;
use App\Models\Pelatihan;
use App\Models\Pengaduan;
use App\Models\Refleksi;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function pengaduan_masyarakat_controller()
    {
        return view('admin.pengaduan');
    }
    public function list_pengaduan_masyarakat()
    {
        $today = Carbon::now()->toDateString();

        $data = Pengaduan::whereDate('created_at', $today)
            ->where('jenis', 'P')
            ->orderBy('id', 'desc')
            ->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                $no = 0;
                return '' . $no++ . '';
            })
            ->addColumn('tanggal', function ($data) {
                $tanggal = Carbon::parse($data->created_at)->format('d-m-Y');
                $waktu = Carbon::parse($data->created_at)->format('H:i');

                return " <span>$tanggal</span><br>
            <span>$waktu</span>";
            })
            ->addColumn('token', function ($data) {
                return '' . $data->token . '';
            })
            ->addColumn('nama', function ($data) {
                return "$data->nama";
            })
            ->addColumn('nohp', function ($data) {
                return '' . $data->nohp . '';
            })
            ->addColumn('email', function ($data) {
                return '' . $data->email . '';
            })
            ->addColumn('status', function ($data) {
                $waktu_pengajuan = Carbon::parse($data->created_at)->format('d-m-Y(H:i)');
                $ditolak = "none";
                $statustolak = "";
                $bgcolortolak = "";
                $iclasstolak = "";
                $color = "#fff";
                $color1 = "#fff";
                if ($data->status_selesai) {
                    if ($data->status == "Ditolak Verifikator") {

                        $statustolak = $data->status;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $statustolak = $data->status_ka;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status_ka == "Di Tangguhkan") {
                        $statustolak = $data->status_ka;
                        $ditolak = "";
                        $bgcolortolak = "#dbd821";
                        $iclasstolak = "bi-x-lg";
                        $color = "#0a0a0a";
                    }
                    if ($data->status_bagian == "Tidak Tertangani") {
                        $statustolak = $data->status_bagian;
                        $ditolak = "";
                        $bgcolortolak = "#dbd821";
                        $iclasstolak = "bi-x-lg";
                        $color = "#0a0a0a";
                    }
                    if ($data->status_bagian == "Ditangani") {
                        $statustolak = $data->status_bagian;
                        $ditolak = "";
                        $bgcolortolak = "#28a745";
                        $iclasstolak = "bi-x-lg";
                    }
                    $status = $data->status_selesai;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_bagian) {
                    if ($data->status_bagian == "Ditangani") {
                        $status = $data->status_bagian;
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    } else {
                        $status = $data->status_bagian;
                        $bgcolor = "#dbd821";
                        $iclass = "bi-check-lg";
                        $color1 = "#0a0a0a";
                    }
                } elseif ($data->status_pemberian_bagian) {
                    $status = $data->status_pemberian_bagian;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_ka) {
                    $status = $data->status_ka;
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $bgcolor = "#dc3545";
                        $iclass = "bi-x-lg";
                    } elseif ($data->status_ka == "Di Tangguhkan") {
                        $bgcolor = "#dbd821";
                        $iclass = "bi-stopwatch";
                        $color1 = "#0a0a0a";
                    } else {
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    }
                } elseif ($data->status == "Diverifikasi Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status == "Ditolak Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#dc3545";
                    $iclass = "bi-x-lg";
                } else {
                    $status = $data->status;
                    $bgcolor = "#1667d7";
                    $iclass = "bi-check-lg";
                }




                return "
            <span style='background-color: $bgcolor;color:$color1; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclass'></i> $status
            </span><br><br>
            <span style='display:$ditolak;background-color: $bgcolortolak;color:$color; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclasstolak'></i> $statustolak
            </span>
            ";
            })
            ->addColumn('tombol', function ($data) {
                return "
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Proses' class='btn btn-primary proses' data-id='$data->id'><i class='bi-box-arrow-in-up-right'></i></button>
            </div>
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Detail' class='btn btn-success detail' data-id='$data->id'><i class='bi-list'></i></button>
            </div>";
            })
            ->rawColumns(['tombol', 'status', 'tanggal'])
            ->make(true);
    }
    public function pencarian_pengaduan_masyarakat(Request $request)
    {


        $tanggalawal = $request->input('tanggalawal');
        $tanggalakhir = $request->input('tanggalakhir');
        $nama = $request->input('nama');
        $nohp = $request->input('nohp');
        $email = $request->input('email');
        $token = $request->input('token');
        $status = $request->input('status');

        $query = Pengaduan::query();

        if ($tanggalawal && $tanggalakhir) {
            $awal = Carbon::createFromFormat('d-m-Y', $tanggalawal)
                ->startOfDay()
                ->setTimezone('Asia/Jakarta');

            $akhir = Carbon::createFromFormat('d-m-Y', $tanggalakhir)
                ->endOfDay()
                ->setTimezone('Asia/Jakarta');
            $query->whereBetween('created_at', [$awal, $akhir]);
        }

        if ($nama) {
            $query->where('nama', 'like', '%' . $nama . '%');
        }

        if ($nohp) {
            $query->where('nohp', 'like', '%' . $nohp . '%');
        }

        if ($email) {
            $query->where('email', 'like', '%' . $email . '%');
        }

        if ($token) {
            $query->where('token', 'like', '%' . $token . '%');
        }

        if ($status) {
            $query->where(function ($q) use ($status) {
                $q->where('status', '=', $status)
                    ->orWhere('status_ka', '=', $status)
                    ->orWhere('status_bagian', '=', $status)
                    ->orWhere('status_selesai', '=', $status)
                    ->orWhere('status_pemberian_bagian', '=', $status);
            });
        }



        $data = $query->orderBy('id', 'desc')->where('jenis', 'P')->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                $no = 0;
                return '' . $no++ . '';
            })
            ->addColumn('tanggal', function ($data) {
                $tanggal = Carbon::parse($data->created_at)->format('d-m-Y');
                $waktu = Carbon::parse($data->created_at)->format('H:i');

                return " <span>$tanggal</span><br>
            <span>$waktu</span>";
            })
            ->addColumn('nama', function ($data) {
                return "$data->nama";
            })
            ->addColumn('nohp', function ($data) {
                return '' . $data->nohp . '';
            })
            ->addColumn('token', function ($data) {
                return '' . $data->token . '';
            })
            ->addColumn('email', function ($data) {
                return '' . $data->email . '';
            })
            ->addColumn('status', function ($data) {
                $waktu_pengajuan = Carbon::parse($data->created_at)->format('d-m-Y(H:i)');
                $ditolak = "none";
                $statustolak = "";
                $bgcolortolak = "";
                $iclasstolak = "";
                $color = "#fff";
                $color1 = "#fff";
                if ($data->status_selesai) {
                    if ($data->status == "Ditolak Verifikator") {

                        $statustolak = $data->status;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $statustolak = $data->status_ka;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status_ka == "Di Tangguhkan") {
                        $statustolak = $data->status_ka;
                        $ditolak = "";
                        $bgcolortolak = "#dbd821";
                        $iclasstolak = "bi-x-lg";
                        $color = "#0a0a0a";
                    }
                    if ($data->status_bagian == "Tidak Tertangani") {
                        $statustolak = $data->status_bagian;
                        $ditolak = "";
                        $bgcolortolak = "#dbd821";
                        $iclasstolak = "bi-x-lg";
                        $color = "#0a0a0a";
                    }
                    if ($data->status_bagian == "Ditangani") {
                        $statustolak = $data->status_bagian;
                        $ditolak = "";
                        $bgcolortolak = "#28a745";
                        $iclasstolak = "bi-x-lg";
                    }
                    $status = $data->status_selesai;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_bagian) {
                    if ($data->status_bagian == "Ditangani") {
                        $status = $data->status_bagian;
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    } else {
                        $status = $data->status_bagian;
                        $bgcolor = "#dbd821";
                        $iclass = "bi-check-lg";
                        $color1 = "#0a0a0a";
                    }
                } elseif ($data->status_pemberian_bagian) {
                    $status = $data->status_pemberian_bagian;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_ka) {
                    $status = $data->status_ka;
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $bgcolor = "#dc3545";
                        $iclass = "bi-x-lg";
                    } elseif ($data->status_ka == "Di Tangguhkan") {
                        $bgcolor = "#dbd821";
                        $iclass = "bi-stopwatch";
                        $color1 = "#0a0a0a";
                    } else {
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    }
                } elseif ($data->status == "Diverifikasi Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status == "Ditolak Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#dc3545";
                    $iclass = "bi-x-lg";
                } else {
                    $status = $data->status;
                    $bgcolor = "#1667d7";
                    $iclass = "bi-check-lg";
                }




                return "
            <span style='background-color: $bgcolor;color:$color1; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclass'></i> $status
            </span><br><br>
            <span style='display:$ditolak;background-color: $bgcolortolak;color:$color; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclasstolak'></i> $statustolak
            </span>
            ";
            })
            ->addColumn('tombol', function ($data) {
                return "
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Proses' class='btn btn-primary proses' data-id='$data->id'><i class='bi-box-arrow-in-up-right'></i></button>
            </div>
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Detail' class='btn btn-success detail' data-id='$data->id'><i class='bi-list'></i></button>
            </div>";
            })
            ->rawColumns(['tombol', 'status', 'tanggal'])
            ->make(true);
    }
    public function detail_pengaduan($id)
    {
        $pengaduan = Pengaduan::where('id', $id)->first();
        $bukti = File::where('id_pengaduan', $id)->get();
        $res = ['pengaduan' => $pengaduan, 'bukti' => $bukti];
        return response()->json($res);
    }
    public function unduh_file_bukti_pengaduan($path)
    {
        $filePath = public_path($path);

        if (file_exists($filePath)) {
            return response()->download($filePath);
        }

        return response()->json(['message' => 'File not found.'], 404);
    }

    public function jumlah_pengaduan_masyarakat()
    {
        $pengaduan = Pengaduan::where('jenis', 'P')->count();
        $res = ['jumlah' => $pengaduan];
        return response()->json($res);
    }
    public function status_verifikasi($id)
    {
        $pengaduan
            = Pengaduan::where('id', $id)->first();
        if ($pengaduan->status_selesai == "Selesai") {
            $selesai = "selesai";
        } else {
            $selesai = null;
        }
        if ($pengaduan->status == "Diverifikasi Verifikator") {

            $res = ['status' => 'Diverifikasi', 'verifikator' => 'Diverifikasi', 'keterangan' => $pengaduan->respon_verifikasi, 'id' => $pengaduan->id, 'waktu' => $pengaduan->waktu_verifikasi, 'selesai' => $selesai];
        } elseif ($pengaduan->status == "Ditolak Verifikator") {
            $res = ['status' => 'Ditolak', 'verifikator' => 'Ditolak', 'keterangan' => $pengaduan->respon_verifikasi, 'id' => $pengaduan->id, 'waktu' => $pengaduan->waktu_verifikasi, 'selesai' => $selesai];
        } else {
            $res = ['status' => 'Belum'];
        }
        return response()->json($res);
    }
    public function submit_verifikator(Request $r)
    {
        $data
            = Pengaduan::where('id', $r->id)->first();
        if ($r->status == "Diverifikasi") {
            $data->status = 'Diverifikasi Verifikator';
            $data->respon_verifikasi = $r->keterangan;
            $data->waktu_verifikasi
                = Carbon::now();
            $data->save();
            $status = "Diverifikasi";
        } else {
            $data->status = 'Ditolak Verifikator';
            $data->respon_verifikasi = $r->keterangan;
            $data->status_ka = null;
            $data->respon_ka = null;
            $data->waktu_ka = null;
            $data->status_bagian = null;
            $data->waktu_bagian = null;
            $data->ket_bagian = null;

            $data->status_selesai = "Selesai";
            $data->waktu_verifikasi
                = Carbon::now();
            $data->waktu_selesai
                = Carbon::now();
            $data->bagian = null;
            $data->status_pemberian_bagian = null;
            $data->waktu_pemberian_bagian = null;
            $data->save();
            $status = "Ditolak";
        }
        $res = ['status' => 'berhasil', 'keterangan' => $status, 'id' => $data->id];
        return response()->json($res);
    }
    public function status_disposisi($id)
    {
        $data
            = Pengaduan::where('id', $id)->first();
        if ($data->status_selesai == "Selesai") {
            $res = ['status' => 'selesai', 'data' => $data];
        }
        if ($data->status == "Pengaduan Baru") {
            $res = ['status' => 'belum diverifikasi'];
        }
        if ($data->status == "Diverifikasi Verifikator") {
            $res = ['status' => 'belum'];
        }
        if ($data->status == "Ditolak Verifikator") {
            $res = ['status' => 'ditolak_verifikator'];
        }
        if ($data->status_ka == "Ditolak KA Sub Bag") {
            $res = ['status' => 'ditolak_kasubag', 'data' => $data];
        }
        if ($data->status_ka == "Di Tangguhkan") {
            $res = ['status' => 'ditangguhkan', 'data' => $data];
        }
        if ($data->status_ka == "Diterima KA Sub Bag") {

            $res = ['status' => 'diterima', 'data' => $data];
        }
        if ($data->status_ka == "Di Tindak Lanjuti") {
            $res = ['status' => 'ditindaklanjut', 'data' => $data];
        }
        return response()->json($res);
    }
    public function submit_penerimaan(Request $r)
    {
        $data
            = Pengaduan::where('id', $r->id)->first();
        if ($r->penerimaan == "Diterima") {
            $data->status_ka = 'Diterima KA Sub Bag';
            $data->respon_ka = $r->keterangan;
            $data->waktu_ka
                = Carbon::now();
            $data->save();
            $status = "Berhasil Diterima";
        }
        if ($r->penerimaan == "Ditangguhkan") {
            $data->status_ka = 'Di Tangguhkan';
            $data->respon_ka = $r->keterangan;
            $data->waktu_ka
                = Carbon::now();
            $data->bagian = null;
            $data->status_pemberian_bagian = null;
            $data->waktu_pemberian_bagian = null;
            $data->save();
            $status = "Ditangguhkan";
        }
        if ($r->penerimaan == "Ditolak") {
            $data->status_ka = 'Ditolak KA Sub Bag';
            $data->respon_ka = $r->keterangan;
            $data->waktu_ka
                = Carbon::now();
            $data->status_bagian = null;
            $data->waktu_bagian = null;
            $data->status_selesai = "Selesai";
            $data->waktu_verifikasi
                = Carbon::now();
            $data->waktu_selesai
                = Carbon::now();
            $data->bagian = null;
            $data->status_pemberian_bagian = null;
            $data->waktu_pemberian_bagian = null;
            $data->save();
            $status = "Ditolak";
        }


        $res = ['status' => 'berhasil', 'keterangan' => $status, 'id' => $data->id];
        return response()->json($res);
    }
    public function submit_bagian(Request $r)
    {
        $data
            = Pengaduan::where('id', $r->id)->first();
        $data->status_ka = 'Di Tindak Lanjuti';
        $data->bagian = $r->bagian;
        $data->status_pemberian_bagian = "Diterima Bagian Terkait";
        $data->waktu_pemberian_bagian
            = Carbon::now();
        $data->save();
        $status = "Penugasan Berhasil Diberikan Kepada Bagian Terkait";
        $res = ['status' => 'berhasil', 'keterangan' => $status, 'id' => $data->id];
        return response()->json($res);
    }
    public function status_tindaklanjut($id)
    {
        $pengaduan
            = Pengaduan::where('id', $id)->first();
        $bukti = Bukti::where('id_pengaduan', $pengaduan->id)->get();
        if ($pengaduan->status_selesai == "Selesai") {
            $res = ['status' => 'selesai', 'data' => $pengaduan, 'bukti' => $bukti];
        }

        if ($pengaduan->status_pemberian_bagian == "Diterima Bagian Terkait") {

            if ($pengaduan->status_bagian == "Ditangani" || $pengaduan->status_bagian == "Tidak Tertangani") {
                $res = ['status' => 'ditangani', 'data' => $pengaduan, 'bukti' => $bukti];
            } else {
                $res = ['status' => 'belum ditangani', 'data' => $pengaduan];
            }
        } else {
            $res = ['status' => 'belum diterima'];
        }
        return response()->json($res);
    }
    public function submittindaklanjutbagian(Request $r)
    {
        $pengaduan = Pengaduan::where('id', $r->id)->first();

        $pengaduan->status_bagian = $r->tindaklanjut;
        $pengaduan->ket_bagian = $r->ket_bagian;
        $pengaduan->waktu_bagian
            = Carbon::now();
        $pengaduan->save();

        // Simpan file ke folder public/upload dan data ke model Bukti
        if ($r->hasFile('bukti')) {
            foreach ($r->file('bukti') as $file) {
                $originalName = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $fileName = Str::slug(pathinfo($originalName, PATHINFO_FILENAME)) . '_' . time() . '.' . $extension;

                // Simpan file ke storage dengan nama unik
                $file->move(public_path('upload'), $fileName);

                // Simpan path ke database
                $path = 'upload/' . $fileName;
                $bukti = new Bukti([
                    'id_pengaduan' => $pengaduan->id,
                    'nama_file' => $originalName,
                    'path' => $path
                ]);
                $bukti->save();
            }
        }

        return response()->json(['status' => 'berhasil']);
    }
    public function status_selesai($id)
    {
        $pengaduan
            = Pengaduan::where('id', $id)->first();

        if ($pengaduan->status_selesai == "Selesai") {
            $res = ['status' => 'selesai', 'data' => $pengaduan];
        } else {
            $res = ['status' => 'belum'];
        }
        return response()->json($res);
    }
    public function submitselesai(Request $r)
    {
        $pengaduan = Pengaduan::where('id', $r->id)->first();

        $pengaduan->status_selesai = $r->status;
        $pengaduan->ket_selesai = $r->keterangan;
        $pengaduan->waktu_selesai
            = Carbon::now();
        $pengaduan->save();

        // Simpan file ke folder public/upload dan data ke model Bukti


        return response()->json(['status' => 'berhasil']);
    }
    public function status_track($id)
    {
        $data = Pengaduan::where('id', $id)->first();
        return response()->json($data);
    }
    public function cek($id)
    {
        $data = Pengaduan::where('id', $id)->first();
        if ($data->status_selesai) {
            $status = $data->status_selesai;
            $bgcolor = "#28a745";
            $iclass = "bi-check-lg";
        } elseif ($data->status_bagian) {
            $status = $data->status_bagian;
            $bgcolor = "#28a745";
            $iclass = "bi-check-lg";
        } elseif ($data->status_pemberian_bagian) {
            $status = $data->status_pemberian_bagian;
            $bgcolor = "#28a745";
            $iclass = "bi-check-lg";
        } elseif ($data->status_ka) {
            $status = $data->status_ka;
            if ($data->status_ka == "Ditolak KA Sub Bag") {
                $bgcolor = "#dc3545";
                $iclass = "bi-x-lg";
            } else {
                $bgcolor = "#28a745";
                $iclass = "bi-check-lg";
            }
        } elseif ($data->status == "Diverifikasi Verifikator") {
            $status = $data->status;
            $bgcolor = "#28a745";
            $iclass = "bi-check-lg";
        } elseif ($data->status == "Ditolak Verifikator") {
            $status = $data->status;
            $bgcolor = "#dc3545";
            $iclass = "bi-x-lg";
        } else {
            $status = $data->status;
            $bgcolor = "#1667d7";
            $iclass = "bi-check-lg";
        }

        $res = ['status' => $status, 'bgcolor' => $bgcolor, 'i' => $iclass];
        return response()->json($res);
        $res = ['status' => $status, 'bgcolor' => $bgcolor, 'i' => $iclass];
        return response()->json($res);
    }


    //wbspage

    public function wbs_controller()
    {
        return view('admin.wbs');
    }
    public function list_wbs()
    {
        $today = Carbon::now()->toDateString();

        $data = Pengaduan::whereDate('created_at', $today)
            ->where('jenis', 'W')
            ->orderBy('id', 'desc')
            ->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                $no = 0;
                return '' . $no++ . '';
            })
            ->addColumn('tanggal', function ($data) {
                $tanggal = Carbon::parse($data->created_at)->format('d-m-Y');
                $waktu = Carbon::parse($data->created_at)->format('H:i');

                return " <span>$tanggal</span><br>
            <span>$waktu</span>";
            })
            ->addColumn('token', function ($data) {
                return '' . $data->token . '';
            })
            ->addColumn('nama', function ($data) {
                return "$data->nama";
            })
            ->addColumn('nohp', function ($data) {
                return '' . $data->nohp . '';
            })
            ->addColumn('email', function ($data) {
                return '' . $data->email . '';
            })
            ->addColumn('status', function ($data) {
                $waktu_pengajuan = Carbon::parse($data->created_at)->format('d-m-Y(H:i)');
                $ditolak = "none";
                $statustolak = "";
                $bgcolortolak = "";
                $iclasstolak = "";
                if ($data->status_selesai) {
                    if ($data->status == "Ditolak Verifikator") {

                        $statustolak = $data->status;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $statustolak = $data->status_ka;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    $status = $data->status_selesai;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_bagian) {
                    $status = $data->status_bagian;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_pemberian_bagian) {
                    $status = $data->status_pemberian_bagian;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_ka) {
                    $status = $data->status_ka;
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $bgcolor = "#dc3545";
                        $iclass = "bi-x-lg";
                    } else {
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    }
                } elseif ($data->status == "Diverifikasi Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status == "Ditolak Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#dc3545";
                    $iclass = "bi-x-lg";
                } else {
                    $status = $data->status;
                    $bgcolor = "#1667d7";
                    $iclass = "bi-check-lg";
                }




                return "
            <span style='background-color: $bgcolor;color:#fff; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclass'></i> $status
            </span><br><br>
            <span style='display:$ditolak;background-color: $bgcolortolak;color:#fff; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclasstolak'></i> $statustolak
            </span>
            ";
            })
            ->addColumn('tombol', function ($data) {
                return "
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Proses' class='btn btn-primary proses' data-id='$data->id'><i class='bi-box-arrow-in-up-right'></i></button>
            </div>
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Detail' class='btn btn-success detail' data-id='$data->id'><i class='bi-list'></i></button>
            </div>";
            })
            ->rawColumns(['tombol', 'status', 'tanggal'])
            ->make(true);
    }
    public function pencarian_wbs(Request $request)
    {


        $tanggalawal = $request->input('tanggalawal');
        $tanggalakhir = $request->input('tanggalakhir');
        $nama = $request->input('nama');
        $nohp = $request->input('nohp');
        $email = $request->input('email');
        $token = $request->input('token');
        $status = $request->input('status');

        $query = Pengaduan::query();

        if ($tanggalawal && $tanggalakhir) {
            $awal = Carbon::createFromFormat('d-m-Y', $tanggalawal)
                ->startOfDay()
                ->setTimezone('Asia/Jakarta');

            $akhir = Carbon::createFromFormat('d-m-Y', $tanggalakhir)
                ->endOfDay()
                ->setTimezone('Asia/Jakarta');
            $query->whereBetween('created_at', [$awal, $akhir]);
        }

        if ($nama) {
            $query->where('nama', 'like', '%' . $nama . '%');
        }

        if ($nohp) {
            $query->where('nohp', 'like', '%' . $nohp . '%');
        }

        if ($email) {
            $query->where('email', 'like', '%' . $email . '%');
        }

        if ($token) {
            $query->where('token', 'like', '%' . $token . '%');
        }

        if ($status) {
            $query->where(function ($q) use ($status) {
                $q->where('status', '=', $status)
                    ->orWhere('status_ka', '=', $status)
                    ->orWhere('status_bagian', '=', $status)
                    ->orWhere('status_selesai', '=', $status)
                    ->orWhere('status_pemberian_bagian', '=', $status);
            });
        }



        $data = $query->orderBy('id', 'desc')->where('jenis', 'W')->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                $no = 0;
                return '' . $no++ . '';
            })
            ->addColumn('tanggal', function ($data) {
                $tanggal = Carbon::parse($data->created_at)->format('d-m-Y');
                $waktu = Carbon::parse($data->created_at)->format('H:i');

                return " <span>$tanggal</span><br>
            <span>$waktu</span>";
            })
            ->addColumn('nama', function ($data) {
                return "$data->nama";
            })
            ->addColumn('nohp', function ($data) {
                return '' . $data->nohp . '';
            })
            ->addColumn('token', function ($data) {
                return '' . $data->token . '';
            })
            ->addColumn('email', function ($data) {
                return '' . $data->email . '';
            })
            ->addColumn('status', function ($data) {
                $waktu_pengajuan = Carbon::parse($data->created_at)->format('d-m-Y(H:i)');
                $ditolak = "none";
                $statustolak = "";
                $bgcolortolak = "";
                $iclasstolak = "";
                if ($data->status_selesai) {
                    if ($data->status_bagian == "Tidak Tertangani") {
                        $statustolak = $data->status_bagian;
                        $bgcolortolak = "#ebba34";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status == "Ditolak Verifikator") {

                        $statustolak = $data->status;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $statustolak = $data->status_ka;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    $status = $data->status_selesai;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_bagian) {
                    if ($data->status_bagian == "Tidak Tertangani") {
                        $status = $data->status_bagian;
                        $bgcolor = "#ebba34";
                        $iclass = "bi-x-lg";
                    } else {
                        $status = $data->status_bagian;
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    }
                } elseif ($data->status_pemberian_bagian) {
                    $status = $data->status_pemberian_bagian;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_ka) {
                    $status = $data->status_ka;
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $bgcolor = "#dc3545";
                        $iclass = "bi-x-lg";
                    } else {
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    }
                } elseif ($data->status == "Diverifikasi Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status == "Ditolak Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#dc3545";
                    $iclass = "bi-x-lg";
                } else {
                    $status = $data->status;
                    $bgcolor = "#1667d7";
                    $iclass = "bi-check-lg";
                }




                return "
            <span style='background-color: $bgcolor;color:#fff; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclass'></i> $status
            </span><br><br>
            <span style='display:$ditolak;background-color: $bgcolortolak;color:#fff; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclasstolak'></i> $statustolak
            </span>
            ";
            })
            ->addColumn('tombol', function ($data) {
                return "
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Proses' class='btn btn-primary proses' data-id='$data->id'><i class='bi-box-arrow-in-up-right'></i></button>
            </div>
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Detail' class='btn btn-success detail' data-id='$data->id'><i class='bi-list'></i></button>
            </div>";
            })
            ->rawColumns(['tombol', 'status', 'tanggal'])
            ->make(true);
    }
    public function jumlah_wbs()
    {
        $pengaduan = Pengaduan::where('jenis', 'W')->count();
        $res = ['jumlah' => $pengaduan];
        return response()->json($res);
    }
    public function pelatihan()
    {
        return view('admin.pelatihan');
    }
    public function list_pelatihan()
    {


        $data = Pelatihan::orderBy('id', 'desc')
            ->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                $no = 0;
                return '' . $no++ . '';
            })
            ->addColumn('tanggal', function ($data) {
                $tanggal_mulai = Carbon::parse($data->tanggal_mulai)->format('d-m-Y');
                $tanggal_berakhir = Carbon::parse($data->tanggal_berakhir)->format('d-m-Y');

                return " <span>$tanggal_mulai</span><br>
            s/d<br>
            <span>$tanggal_berakhir</span>";
            })
            ->addColumn('token', function ($data) {
                return '' . $data->token . '';
            })
            ->addColumn('mot', function ($data) {
                return '' . $data->mot . '';
            })
            ->addColumn('nama_pelatihan', function ($data) {
                return "$data->nama_pelatihan";
            })

            ->addColumn('tombol', function ($data) {
                return "

            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Detail' class='btn btn-success detail' data-id='$data->id'><i class='bi-list'></i></button>
            </div>";
            })
            ->rawColumns(['tombol', 'tanggal'])
            ->make(true);
    }
    public function tambah_pelatihan(Request $r)
    {
        $data = new Pelatihan;

        $data->nama_pelatihan = $r->nama_pelatihan;
        $data->mot = $r->mot;
        $data->tanggal_mulai = Carbon::createFromFormat('d-m-Y', $r->tanggalawal);
        $data->tanggal_berakhir = Carbon::createFromFormat('d-m-Y', $r->tanggalakhir);
        $data->save();
        return response()->json(['pesan' => 'sukses']);
    }
    public function jumlah_pelatihan()
    {
        $pengaduan = Pelatihan::count();
        $res = ['jumlah' => $pengaduan];
        return response()->json($res);
    }
    public function detail_pelatihan($id)
    {
        $data = Pelatihan::where('id', $id)->first();

        return response()->json($data);
    }
    public function update_pelatihan(Request $r)
    {

        $data = Pelatihan::where('id', $r->id)->first();
        $data->nama_pelatihan = $r->nama_pelatihan;
        $data->mot = $r->mot;
        $data->tanggal_mulai = Carbon::createFromFormat('d-m-Y', $r->tanggalawal);
        $data->tanggal_berakhir = Carbon::createFromFormat('d-m-Y', $r->tanggalakhir);
        $data->save();
        return response()->json(['pesan' => 'sukses']);
    }
    public function hapus_pelatihan(Request $r)
    {
        $data = Pelatihan::where('id', $r->id)->first();
        $data->delete();
        return response()->json(['pesan' => 'sukses']);
    }


    public function refleksi_controller()
    {
        return view('admin.refleksi');
    }
    public function jumlah_refleksi()
    {
        $pengaduan = Pengaduan::where('jenis', 'R')->count();
        $res = ['jumlah' => $pengaduan];
        return response()->json($res);
    }
    public function list_refleksi()
    {
        $today = Carbon::now()->toDateString();

        $data = Pengaduan::whereDate('created_at', $today)
            ->where('jenis', 'R')
            ->orderBy('id', 'desc')
            ->get();


        return DataTables::of($data)

            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                $no = 0;
                return '' . $no++ . '';
            })
            ->addColumn('tanggal', function ($data) {
                $tanggal = Carbon::parse($data->created_at)->format('d-m-Y');
                $waktu = Carbon::parse($data->created_at)->format('H:i');

                return " <span>$tanggal</span><br>
            <span>$waktu</span>";
            })
            ->addColumn('token', function ($data) {
                return '' . $data->token . '';
            })
            ->addColumn('nama_pelatihan', function ($data) {
                $refleksi1 = Refleksi::where('id', $data->id_refleksi)->first();
                $pelatihan1 = Pelatihan::where('id', $refleksi1->id_pelatihan)->first();
                return "$pelatihan1->nama_pelatihan";
            })
            ->addColumn('mot', function ($data) {
                $refleksi = Refleksi::where('id', $data->id_refleksi)->first();
                $pelatihan = Pelatihan::where('id', $refleksi->id_pelatihan)->first();
                return '' . $pelatihan->mot . '';
            })
            ->addColumn('email', function ($data) {
                return '' . $data->email . '';
            })
            ->addColumn('status', function ($data) {
                $waktu_pengajuan = Carbon::parse($data->created_at)->format('d-m-Y(H:i)');
                $ditolak = "none";
                $statustolak = "";
                $bgcolortolak = "";
                $iclasstolak = "";
                $color = "#fff";
                $color1 = "#fff";
                if ($data->status_selesai) {
                    if ($data->status == "Ditolak Verifikator") {

                        $statustolak = $data->status;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $statustolak = $data->status_ka;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status_ka == "Di Tangguhkan") {
                        $statustolak = $data->status_ka;
                        $ditolak = "";
                        $bgcolortolak = "#dbd821";
                        $iclasstolak = "bi-x-lg";
                        $color = "#0a0a0a";
                    }
                    if ($data->status_bagian == "Tidak Tertangani") {
                        $statustolak = $data->status_bagian;
                        $ditolak = "";
                        $bgcolortolak = "#dbd821";
                        $iclasstolak = "bi-x-lg";
                        $color = "#0a0a0a";
                    }
                    if ($data->status_bagian == "Ditangani") {
                        $statustolak = $data->status_bagian;
                        $ditolak = "";
                        $bgcolortolak = "#28a745";
                        $iclasstolak = "bi-x-lg";
                    }
                    $status = $data->status_selesai;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_bagian) {
                    if ($data->status_bagian == "Ditangani") {
                        $status = $data->status_bagian;
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    } else {
                        $status = $data->status_bagian;
                        $bgcolor = "#dbd821";
                        $iclass = "bi-check-lg";
                        $color1 = "#0a0a0a";
                    }
                } elseif ($data->status_pemberian_bagian) {
                    $status = $data->status_pemberian_bagian;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_ka) {
                    $status = $data->status_ka;
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $bgcolor = "#dc3545";
                        $iclass = "bi-x-lg";
                    } elseif ($data->status_ka == "Di Tangguhkan") {
                        $bgcolor = "#dbd821";
                        $iclass = "bi-stopwatch";
                        $color1 = "#0a0a0a";
                    } else {
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    }
                } elseif ($data->status == "Diverifikasi Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status == "Ditolak Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#dc3545";
                    $iclass = "bi-x-lg";
                } else {
                    $status = $data->status;
                    $bgcolor = "#1667d7";
                    $iclass = "bi-check-lg";
                }




                return "
            <span style='background-color: $bgcolor;color:$color1; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclass'></i> $status
            </span><br><br>
            <span style='display:$ditolak;background-color: $bgcolortolak;color:$color; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclasstolak'></i> $statustolak
            </span>
            ";
            })
            ->addColumn('tombol', function ($data) {
                return "
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Proses' class='btn btn-primary proses' data-id='$data->id'><i class='bi-box-arrow-in-up-right'></i></button>
            </div>
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Detail' class='btn btn-success detail' data-id='$data->id'><i class='bi-list'></i></button>
            </div>";
            })
            ->rawColumns(['tombol', 'status', 'tanggal'])
            ->make(true);
    }
    public function pencarian_refleksi(Request $request)
    {


        $tanggalawal = $request->input('tanggalawal');
        $tanggalakhir = $request->input('tanggalakhir');
        $nama = $request->input('nama');
        $nohp = $request->input('nohp');
        $email = $request->input('email');
        $token = $request->input('token');
        $status = $request->input('status');

        $query = Pengaduan::query();

        if ($tanggalawal && $tanggalakhir) {
            $awal = Carbon::createFromFormat('d-m-Y', $tanggalawal)
                ->startOfDay()
                ->setTimezone('Asia/Jakarta');

            $akhir = Carbon::createFromFormat('d-m-Y', $tanggalakhir)
                ->endOfDay()
                ->setTimezone('Asia/Jakarta');
            $query->whereBetween('created_at', [$awal, $akhir]);
        }

        if ($nama) {
            $query->where('nama', 'like', '%' . $nama . '%');
        }

        if ($nohp) {
            $query->where('nohp', 'like', '%' . $nohp . '%');
        }

        if ($email) {
            $query->where('email', 'like', '%' . $email . '%');
        }

        if ($token) {
            $query->where('token', 'like', '%' . $token . '%');
        }

        if ($status) {
            $query->where(function ($q) use ($status) {
                $q->where('status', '=', $status)
                    ->orWhere('status_ka', '=', $status)
                    ->orWhere('status_bagian', '=', $status)
                    ->orWhere('status_selesai', '=', $status)
                    ->orWhere('status_pemberian_bagian', '=', $status);
            });
        }



        $data = $query->orderBy('id', 'desc')->where('jenis', 'R')->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                $no = 0;
                return '' . $no++ . '';
            })
            ->addColumn('tanggal', function ($data) {
                $tanggal = Carbon::parse($data->created_at)->format('d-m-Y');
                $waktu = Carbon::parse($data->created_at)->format('H:i');

                return " <span>$tanggal</span><br>
            <span>$waktu</span>";
            })
            ->addColumn('nama_pelatihan', function ($data) {
                $refleksi = Refleksi::where('id', $data->id_refleksi)->first();
                $pelatihan = Pelatihan::where('id', $refleksi->id_pelatihan)->first();
                return "$pelatihan->nama_pelatihan";
            })
            ->addColumn('mot', function ($data) {
                $refleksi = Refleksi::where('id', $data->id_refleksi)->first();
                $pelatihan = Pelatihan::where('id', $refleksi->id_pelatihan)->first();
                return '' . $pelatihan->mot . '';
            })
            ->addColumn('token', function ($data) {
                return '' . $data->token . '';
            })
            ->addColumn('email', function ($data) {
                return '' . $data->email . '';
            })
            ->addColumn('status', function ($data) {
                $waktu_pengajuan = Carbon::parse($data->created_at)->format('d-m-Y(H:i)');
                $ditolak = "none";
                $statustolak = "";
                $bgcolortolak = "";
                $iclasstolak = "";
                $color = "#fff";
                $color1 = "#fff";
                if ($data->status_selesai) {
                    if ($data->status == "Ditolak Verifikator") {

                        $statustolak = $data->status;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $statustolak = $data->status_ka;
                        $ditolak = "";
                        $bgcolortolak = "#dc3545";
                        $iclasstolak = "bi-x-lg";
                    }
                    if ($data->status_ka == "Di Tangguhkan") {
                        $statustolak = $data->status_ka;
                        $ditolak = "";
                        $bgcolortolak = "#dbd821";
                        $iclasstolak = "bi-x-lg";
                        $color = "#0a0a0a";
                    }
                    if ($data->status_bagian == "Tidak Tertangani") {
                        $statustolak = $data->status_bagian;
                        $ditolak = "";
                        $bgcolortolak = "#dbd821";
                        $iclasstolak = "bi-x-lg";
                        $color = "#0a0a0a";
                    }
                    if ($data->status_bagian == "Ditangani") {
                        $statustolak = $data->status_bagian;
                        $ditolak = "";
                        $bgcolortolak = "#28a745";
                        $iclasstolak = "bi-x-lg";
                    }
                    $status = $data->status_selesai;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_bagian) {
                    if ($data->status_bagian == "Ditangani") {
                        $status = $data->status_bagian;
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    } else {
                        $status = $data->status_bagian;
                        $bgcolor = "#dbd821";
                        $iclass = "bi-check-lg";
                        $color1 = "#0a0a0a";
                    }
                } elseif ($data->status_pemberian_bagian) {
                    $status = $data->status_pemberian_bagian;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status_ka) {
                    $status = $data->status_ka;
                    if ($data->status_ka == "Ditolak KA Sub Bag") {
                        $bgcolor = "#dc3545";
                        $iclass = "bi-x-lg";
                    } elseif ($data->status_ka == "Di Tangguhkan") {
                        $bgcolor = "#dbd821";
                        $iclass = "bi-stopwatch";
                        $color1 = "#0a0a0a";
                    } else {
                        $bgcolor = "#28a745";
                        $iclass = "bi-check-lg";
                    }
                } elseif ($data->status == "Diverifikasi Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#28a745";
                    $iclass = "bi-check-lg";
                } elseif ($data->status == "Ditolak Verifikator") {
                    $status = $data->status;
                    $bgcolor = "#dc3545";
                    $iclass = "bi-x-lg";
                } else {
                    $status = $data->status;
                    $bgcolor = "#1667d7";
                    $iclass = "bi-check-lg";
                }




                return "
            <span style='background-color: $bgcolor;color:$color1; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclass'></i> $status
            </span><br><br>
            <span style='display:$ditolak;background-color: $bgcolortolak;color:$color; border-radius: 10px; padding: 5px 10px;'>
            <i class='$iclasstolak'></i> $statustolak
            </span>
            ";
            })
            ->addColumn('tombol', function ($data) {
                return "
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Proses' class='btn btn-primary proses' data-id='$data->id'><i class='bi-box-arrow-in-up-right'></i></button>
            </div>
            <div class='btn-group' role='group' aria-label='Third group'>
                <button type='button' title='Detail' class='btn btn-success detail' data-id='$data->id'><i class='bi-list'></i></button>
            </div>";
            })
            ->rawColumns(['tombol', 'status', 'tanggal'])
            ->make(true);
    }
}
