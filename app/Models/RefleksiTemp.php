<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefleksiTemp extends Model
{
    use HasFactory;
    protected $table = 'refleksi_temp';
    protected $guarded = [];
}
