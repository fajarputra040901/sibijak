<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileRefleksi extends Model
{
    use HasFactory;
    protected $table = 'file_refleksi';
    protected $guarded = [];
}
