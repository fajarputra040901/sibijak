  @extends('master.inner')

  @section('content')
      <section class="inner-page">
          <div class="container">
              <div class="card">
                  <div class="card-body">
                      <h3 style="text-align: center">Refleksi</h3>
                      <br>
                      <div class="card">
                          <div class="card-body">

                              <div class="card" style="text-align: center">
                                  <div class="card-body">
                                      <h5>Masukan Token Pelatihan</h5>
                                      <br>
                                      <form action="" id="formtoken">
                                          <div class="d-flex justify-content-center align-items-center  mb-3 row">

                                              <div class="col-sm-4">
                                                  <input type="text" placeholder="Masukan Token" class="form-control"
                                                      name="token" id="token">
                                              </div>

                                          </div>
                                          <div class="d-flex justify-content-center align-items-center">
                                              <button class="btn btn-primary" id="masukantoken">Submit</button>
                                          </div>
                                      </form>

                                      <br>
                                      <p><i><b>Dapatkan Token Dari Petugas Pelatihan Untuk Mengisi Refleksi</b></i></p>

                                  </div>
                              </div>
                              <div class="card mt-3" id="pelatihan" style="display: none">
                                  <div class="card-body">
                                      <h3 class="mb-3" style="text-align: center">Pelatihan</h3>
                                      <table class="table">
                                          <tr>
                                              <td>Tanggal Pelatihan</td>
                                              <td><span id="tanggalmulai"></span> s/d <span id="tanggalberakhir"></span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>Nama Pelatihan</td>
                                              <td><span id="nama_pelatihan"></span></td>
                                          </tr>
                                          <tr>
                                              <td>MOT</td>
                                              <td><span id="mot"></span></td>
                                          </tr>
                                          <tr>
                                              <td>Token</td>
                                              <td><span id="token_pelatihan"></span></td>
                                          </tr>
                                      </table>
                                  </div>

                              </div>
                              <div class="card mt-3"id="tambah_refleksi" style="display: none">
                                  <div class="card-body">
                                      <div class="mt-3 mb-2" style="text-align: center">
                                          <h5>Tambah Refleksi</h5>
                                          <button class="btn btn-primary mt-4" id="tambah">
                                              Tambah Refleksi
                                          </button>
                                      </div>
                                  </div>
                              </div>

                              <div class="card mt-3" id="hasil_refleksi" style="display: none">
                                  <div class="card-body">
                                      <h3 style="text-align: center">Hasil Refleksi</h3>

                                      <div class="row mt-3" id="listrefleksi">
                                          <!-- Card 1 -->


                                          <!-- Card 2 -->

                                      </div>
                                  </div>
                              </div>

                          </div>
                      </div>



                  </div>
              </div>
          </div>
          <div class="modal fade" id="modaltambah" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
              aria-labelledby="modaltambahLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-scrollable modal-xl">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title" id="modaltambahLabel">Tambah Refleksi</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                          <form action="" id="formtambah">
                              <input type="text" id="id_tambah" hidden>
                              <div class="mb-3 row">
                                  <label class="col-sm-2 col-form-label">Tanggal Refleksi
                                  </label>
                                  <div class="col-sm-8">
                                      <input type="text" autocomplete="off" data-toggle="datepicker" class="form-control"
                                          id="tanggal" name="tanggal" placeholder="DD-MM-YYYY">
                                  </div>

                              </div>
                              <div class="mb-3 row">
                                  <label for="uraian" class="col-sm-2 col-form-label">Uraian Refleksi</label>
                                  <div class="col-sm-8">
                                      <textarea name="uraian" id="uraian" class="form-control" cols="50"></textarea>
                                      <div class="valid-feedback" style="display: block">
                                          Uraikan Sejelas dan Sedetail Mungkin Serta Mencakup 5W1H
                                      </div>
                                  </div>
                              </div>

                              <button id="submittambah" class="btn btn-primary mt-4">Tambah</button>
                          </form>
                          <div class="mt-4" style="text-align: center">
                              <h4>Hasil Refleksi</h4>
                              <p><i><b>Uraian Refleksi AKan Ditampilkan Disini Setelah Disubmit, Dapat Menambahkan Beberapa
                                          Uraian</b></i></p>

                          </div>
                          <div class="table-responsive">
                              <table id="tabel_refleksi" class="table" style="width: 100%">

                                  <tbody>
                                      <!-- Data akan dimasukkan di sini -->
                                  </tbody>
                              </table>
                          </div>
                      </div>
                      <div class="modal-footer">
                          <button type="button" id="kirim_refleksi" class="btn btn-secondary">Kirim Refleksi</button>

                      </div>
                  </div>
              </div>
          </div>

          <div class="modal fade" id="modaldetail" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
              aria-labelledby="modaldetailLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-scrollable modal-xl">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title" id="modaldetailLabel">Detail Refleksi</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                          <div class="mb-3 row">
                              <label class="col-sm-2 col-form-label">Tanggal Refleksi
                              </label>
                              <div class="col-sm-8">
                                  <input type="text" readonly autocomplete="off" class="form-control"
                                      id="tanggal_detail" name="tanggal_detail" placeholder="DD-MM-YYYY">
                              </div>
                          </div>
                          <div class="table-responsive">
                              <table id="tabel_refleksi_detail" class="table" style="width: 100%">

                                  <tbody>

                                  </tbody>
                              </table>
                          </div>

                          <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal fade" id="modaldetailitem" data-bs-backdrop="static" data-bs-keyboard="false"
              tabindex="-1" aria-labelledby="modaldetailLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-scrollable modal-xl">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title" id="modaldetailLabel">Status Refleksi</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                          <div class="mt-3">
                              <table class="table">
                                  <tr>
                                      <th>Waktu</th>
                                      <th>Status</th>
                                      <th>Keterangan</th>
                                  </tr>

                                  <tr id="trpengaduan" style="display: none">
                                      <td><span id="waktu_pengaduan"></span></td>
                                      <td><span id="status_pengaduan"></span></td>
                                      <td><span id="ket_pengaduan"></span></td>
                                  </tr>
                                  <tr id="trverifikasi" style="display: none">
                                      <td><span id="waktu_verifikasi_status"></span></td>
                                      <td><span id="verifikasi_status"></span></td>
                                      <td id="ket_verifikasi_status"></td>

                                  </tr>
                                  <tr id="trka" style="display: none">
                                      <td><span id="waktu_ka_status"></span></td>
                                      <td><span id="ka_status"></span></td>
                                      <td id="ket_ka_status"></td>
                                  </tr>
                                  <tr id="trbagian" style="display: none">
                                      <td><span id="waktu_bagian_status"></span></td>
                                      <td><span id="bagian_status"></span></td>
                                      <td id="ket_bagian_status"></td>
                                  </tr>
                                  <tr id="trselesai" style="display: none">
                                      <td><span id="waktu_selesai_status"></span></td>
                                      <td><span id="selesai_status"></span></td>
                                      <td><span id="ket_selesai_status"></span></td>
                                  </tr>

                              </table>
                          </div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <script src="{{ asset('date/js/datepicker.js') }}"></script>
      <script>
          $(document).ready(function() {
              function formatDate(date) {
                  var day = String(date.getDate()).padStart(2, '0');
                  var month = String(date.getMonth() + 1).padStart(2, '0'); // January is 0!
                  var year = date.getFullYear();
                  var hours = String(date.getHours()).padStart(2, '0');
                  var minutes = String(date.getMinutes()).padStart(2, '0');

                  return day + '-' + month + '-' + year + ' (' + hours + ':' + minutes + ')';
              }

              function status_track(id) {
                  $.ajax({
                      url: '/status_track/' + id,
                      type: 'GET',
                      dataType: 'json',
                      success: function(res) {
                          $("#trpengaduan").attr("style", "display:null");
                          var time_pengaduan = new Date(res.created_at);
                          var tanggal_pengaduan = formatDate(time_pengaduan);
                          $('#waktu_pengaduan').text(tanggal_pengaduan);
                          $('#status_pengaduan').text("Pengaduan Baru");
                          $('#ket_pengaduan').text("Sedang Di Proses");

                          if (res.status !== null) {
                              if (res.status !== "Pengaduan Baru") {
                                  $('#ket_pengaduan').text("Dikirim");
                                  $("#trverifikasi").attr("style", "display:null");
                                  var time_verifikasi = new Date(res.waktu_verifikasi);
                                  var tanggal_verifikasi = formatDate(time_verifikasi);
                                  $('#waktu_verifikasi_status').text(tanggal_verifikasi);
                                  $('#verifikasi_status').text(res.status);
                                  $('#ket_verifikasi_status').html('<span>' + res.respon_verifikasi +
                                      '</span>' +
                                      '<br><span><i>Dilakukan Oleh Verifikator</i></span>');
                              }

                          }
                          if (res.status_ka !== null) {
                              $("#trka").attr("style", "display:null");
                              var time_ka = new Date(res.waktu_ka);
                              var tanggal_ka = formatDate(time_ka);
                              $('#waktu_ka_status').text(tanggal_ka);
                              $('#ka_status').text(res.status_ka);
                              $('#ket_ka_status').html('<span>' + res.respon_ka + '</span>' +
                                  '<br><span><i>Dilakukan Oleh KA Sub Bagian</i></span>');
                          }
                          if (res.status_bagian !== null) {
                              $("#trbagian").attr("style", "display:null");
                              var time_bagian = new Date(res.waktu_pemberian_bagian);
                              var tanggal_bagian = formatDate(time_bagian);
                              $('#waktu_bagian_status').text(tanggal_bagian);
                              $('#bagian_status').text(res.status_pemberian_bagian);
                              $('#ket_bagian_status').html(
                                  '<span>KA Sub Bag Sudah Melakukan Penugasan Kepada Bagian ' + res
                                  .bagian + '</span>');
                          }
                          if (res.status_bagian !== null) {
                              $("#trbagian").attr("style", "display:null");
                              var time_bagian = new Date(res.waktu_bagian);
                              var tanggal_bagian = formatDate(time_bagian);
                              $('#waktu_bagian_status').text(tanggal_bagian);
                              $('#bagian_status').text(res.status_bagian);
                              $('#ket_bagian_status').html('<span>' + res.ket_bagian + '</span>' +
                                  '<br><span><i>Dilakukan Oleh Bagian ' + res
                                  .bagian + '</i></span>');
                          }
                          if (res.status_selesai !== null) {
                              $("#trselesai").attr("style", "display:null");
                              var time_selesai = new Date(res.waktu_selesai);
                              var tanggal_selesai = formatDate(time_selesai);
                              $('#waktu_selesai_status').text(tanggal_selesai);
                              $('#selesai_status').text(res.status_selesai);
                              $('#ket_selesai_status').text(res.ket_selesai);
                          }






                      },
                      error: function(xhr, status, error) {
                          console.error(xhr.responseText); // Menampilkan pesan kesalahan dalam konsol
                      }
                  });
              }
              $('#kirim_refleksi').click(function() {
                  togglePreloader(true)
                  var id_tambah = $('#id_tambah').val();
                  var tanggal = $('#tanggal').val();
                  if (tanggal) {
                      var formData = new FormData();
                      formData.append('id_tambah', id_tambah);
                      formData.append('tanggal', tanggal);

                      // Menambahkan file ke FormData


                      // Mendapatkan CSRF token dari meta tag
                      var csrfToken = $('meta[name="csrf-token"]').attr('content');

                      // Mengirim data menggunakan AJAX
                      $.ajax({

                          type: 'post',
                          url: '/tambah_refleksi',
                          data: formData,
                          headers: {
                              'X-CSRF-TOKEN': csrfToken,
                          },
                          contentType: false,
                          processData: false,
                          dataType: 'json',
                          success: function(res) {

                              togglePreloader(false)

                              if (res.status == "sudah") {
                                  Swal.fire({
                                      icon: 'error',
                                      title: 'Refleksi Dengan Tanggal Tersebut Sudah Terisi',
                                      text: 'Kirim Refleksi Di Tanggal Berbeda!',
                                  });
                              } else if (res.status == "kosong") {
                                  Swal.fire({
                                      icon: 'error',
                                      title: 'Uraian Refleksi Tidak Ada',
                                      text: 'Input Setidaknya Satu Uraian Refleksi!',
                                  });
                              } else if (res.status == "sukses") {
                                  cek(res.data.token)
                                  $('#modaltambah').modal('hide');
                                  Swal.fire({
                                      icon: 'success',
                                      title: 'Berhasil',
                                      text: 'Refleksi Berhasil Dikrim!',
                                  });
                              }

                          },
                          error: function(error) {
                              togglePreloader(false)
                              // Handle error
                              console.log(error);
                              Swal.fire({
                                  icon: 'error',
                                  title: 'Oops...',
                                  text: 'Terjadi kesalahan. Silakan coba lagi!',
                              });
                          }
                      });

                  } else {
                      togglePreloader(false)
                      Swal.fire({
                          icon: 'error',
                          title: 'Tanggal Refleksi Tidak Boleh Kosong',
                          text: 'Coba Periksa Kembali!',
                      });

                  }



              });
              $(document).on('click', '.hapus_refleksi', function() {
                  var id = $(this).data('id');
                  var id_tambah = $('#id_tambah').val();

                  // Menampilkan SweetAlert2 untuk konfirmasi penghapusan
                  Swal.fire({
                      title: 'Apakah Anda yakin?',
                      text: "Data Akan Di Hapus!",
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Ya, hapus!',
                      cancelButtonText: 'Batal'
                  }).then((result) => {
                      if (result.isConfirmed) {
                          togglePreloader(true);

                          // Membuat FormData
                          var formData = new FormData();
                          formData.append('id', id);

                          // Mendapatkan CSRF token dari meta tag
                          var csrfToken = $('meta[name="csrf-token"]').attr('content');

                          // Mengirim data menggunakan AJAX
                          $.ajax({
                              type: 'post',
                              url: '/hapus_refleksi_temp',
                              data: formData,
                              headers: {
                                  'X-CSRF-TOKEN': csrfToken,
                              },
                              contentType: false,
                              processData: false,
                              dataType: 'json',
                              success: function(res) {
                                  pengaduan(id_tambah);

                                  togglePreloader(false);
                                  Swal.fire({
                                      icon: 'success',
                                      title: 'Berhasil',
                                      text: 'Berhasil Dihapus!',
                                  });

                                  console.log(res);
                              },
                              error: function(error) {
                                  togglePreloader(false);
                                  // Handle error
                                  console.log(error);
                                  Swal.fire({
                                      icon: 'error',
                                      title: 'Oops...',
                                      text: 'Terjadi kesalahan. Silakan coba lagi!',
                                  });
                              }
                          });
                      }
                  });
              });
              $(document).on('click', '.detail_refleksi_item', function() {
                  var id = $(this).data('id');
                  status_track(id);
                  $("#tanggal_detail_item").val(id);
                  $('#modaldetailitem').modal('show');

              });
              $(document).on('click', '.detail', function() {
                  togglePreloader(true);
                  var id = $(this).data('id');
                  $.ajax({
                      type: 'get',
                      url: '/detail_refleksi_temp/' + id,

                      success: function(res) {
                          togglePreloader(false);
                          $("#tanggal_detail").val(res.tanggal);
                          var rows = ''; // Variabel untuk menyimpan baris tabel
                          var index = 1; // Variabel untuk nomor urutan

                          // Memeriksa apakah ada data yang diterima
                          if (res.data && res.data.length > 0) {
                              $.each(res.data, function(_, refleksi) {
                                  // Menambahkan baris tabel untuk setiap objek refleksi
                                  rows += '<tr>' +
                                      '<td>' + index + '</td>' +
                                      '<td>' + refleksi.uraian + '</td>' +
                                      '<td>' + refleksi.status + '</td>' +
                                      '<td>' + refleksi.keterangan + '</td>' +
                                      '<td><button class="btn btn-success detail_refleksi_item" data-id="' +
                                      refleksi.id + '">Detail</button></td>' +
                                      '</tr>';

                                  index++; // Tingkatkan nomor urutan
                              });
                          } else {
                              // Jika tidak ada data, tampilkan pesan kosong
                              rows = '<tr><td colspan="5">Tidak ada data yang tersedia</td></tr>';
                          }

                          // Masukkan baris-baris ke dalam tabel
                          $('#tabel_refleksi_detail tbody').html(rows);

                          // Tampilkan modal
                          $('#modaldetail').modal('show');


                      },
                      error: function(error) {
                          togglePreloader(false);
                          // Handle error
                          console.log(error);
                          Swal.fire({
                              icon: 'error',
                              title: 'Oops...',
                              text: 'Terjadi kesalahan. Silakan coba lagi!',
                          });
                      }
                  });

              });

              $('[data-toggle="datepicker"]').datepicker({
                  autoHide: true,
                  zIndex: 2048,
                  format: 'dd-mm-yyyy',
              });

              function pengaduan(id) {
                  $.ajax({
                      url: '/pengaduan_refleksi/' + id,
                      type: 'GET',
                      dataType: 'json',
                      success: function(res) {
                          if (res.status == 'ada') {
                              var rows = ''; // Variabel untuk menyimpan baris tabel
                              var index = 1; // Variabel untuk nomor urutan

                              $.each(res.data, function(_, refleksi) {
                                  rows += '<tr>' +
                                      '<td style="width: 10%">' + index + '</td>' +
                                      // Nomor urutan
                                      '<td style="width: 700%">' + refleksi.uraian + '</td>' +
                                      '<td style="width: 20%"><button class="btn btn-danger hapus_refleksi" data-id="' +
                                      refleksi.id + '">Hapus</button></td>' +
                                      '</tr>';

                                  index++; // Tingkatkan nomor urutan
                              });

                              // Masukkan baris-baris ke dalam tabel
                              $('#tabel_refleksi tbody').html(rows);
                          } else {
                              // Jika status bukan 'ada', kosongkan isi dari tbody
                              $('#tabel_refleksi tbody').empty();
                          }
                      },
                      error: function(xhr, status, error) {
                          console.error(xhr.responseText); // Menampilkan pesan kesalahan dalam konsol
                      }
                  });
              }

              $('#submittambah').click(function(e) {
                  e.preventDefault(); // Menghentikan default submit form
                  togglePreloader(true)
                  var id_tambah = $('#id_tambah').val();

                  var uraian = $('#uraian').val();


                  // Membuat FormData
                  var formData = new FormData();
                  formData.append('id_tambah', id_tambah);
                  formData.append('uraian', uraian);

                  // Menambahkan file ke FormData


                  // Mendapatkan CSRF token dari meta tag
                  var csrfToken = $('meta[name="csrf-token"]').attr('content');

                  // Mengirim data menggunakan AJAX
                  $.ajax({

                      type: 'post',
                      url: '/tambah_refleksi_temp',
                      data: formData,
                      headers: {
                          'X-CSRF-TOKEN': csrfToken,
                      },
                      contentType: false,
                      processData: false,
                      dataType: 'json',
                      success: function(res) {
                          pengaduan(res.id);
                          $('#uraian').val("");
                          togglePreloader(false)

                          console.log(res);

                      },
                      error: function(error) {
                          togglePreloader(false)
                          // Handle error
                          console.log(error);
                          Swal.fire({
                              icon: 'error',
                              title: 'Oops...',
                              text: 'Terjadi kesalahan. Silakan coba lagi!',
                          });
                      }
                  });
              });
              $('#masukantoken').click(function() {
                  var token = $('#token').val();
                  cek(token)

                  // Mengirim data menggunakan AJAX

              });
              $('#tambah').click(function() {
                  var id = $('#id_tambah').val();
                  $.ajax({
                      url: '/pengaduan_refleksi/' + id,
                      type: 'GET',
                      dataType: 'json',
                      success: function(res) {
                          if (res.status == 'ada') {
                              $('#modaltambah').modal('show');
                              Swal.fire({
                                  icon: 'info',
                                  title: 'Ada Refleksi Yang Belum Dikirim',
                                  text: 'Coba Periksa!',
                              });
                              var rows = ''; // Variabel untuk menyimpan baris tabel
                              var index = 1; // Variabel untuk nomor urutan

                              $.each(res.data, function(_, refleksi) {
                                  rows += '<tr>' +
                                      '<td style="width: 10%">' + index + '</td>' +
                                      // Nomor urutan
                                      '<td style="width: 700%">' + refleksi.uraian +
                                      '</td>' +
                                      '<td style="width: 20%"><button class="btn btn-danger hapus_refleksi" data-id="' +
                                      refleksi.id + '">Hapus</button></td>' +
                                      '</tr>';

                                  index++; // Tingkatkan nomor urutan
                              });

                              // Masukkan baris-baris ke dalam tabel
                              $('#tabel_refleksi tbody').html(rows);
                          } else {
                              $('#tabel_refleksi tbody').empty();
                              $('#modaltambah').modal('show');
                          }
                      },
                      error: function(xhr, status, error) {
                          console.error(xhr
                              .responseText); // Menampilkan pesan kesalahan dalam konsol
                      }
                  });




              });

              function cek(id) {
                  togglePreloader(true)
                  $.ajax({

                      type: 'GET', // Metode HTTP yang digunakan
                      url: '/cek_token_pelatihan', // URL tujuan
                      data: {
                          'token': id,
                      }, // Data yang dikirim
                      dataType: 'json', // Tipe data yang diharapkan dari server
                      success: function(res) {
                          togglePreloader(false)
                          if (res.status == 'ada') {
                              $("#pelatihan").attr("style", "display:");
                              $("#tambah_refleksi").attr("style", "display:");
                              $("#hasil_refleksi").attr("style", "display:");
                              var tanggal_a = new Date(res.data.tanggal_mulai);
                              var tanggal_mulai = tanggalsaja(tanggal_a);
                              $("#tanggalmulai").text(tanggal_mulai);
                              var tanggal_m = new Date(res.data.tanggal_berakhir);
                              var tanggal_berakhir = tanggalsaja(tanggal_m);
                              $("#tanggalberakhir").text(tanggal_berakhir);
                              $("#nama_pelatihan").text(res.data.nama_pelatihan);
                              $("#token_pelatihan").text(res.data.token);
                              $("#id_tambah").val(res.data.id);
                              $("#mot").text(res.data.mot);
                              $('#listrefleksi').empty(); // Mengosongkan konten sebelumnya
                              if (res.refleksi.length > 0) {
                                  res.refleksi.sort(function(a, b) {
                                      return new Date(a.tanggal) - new Date(b.tanggal);
                                  });

                                  // Iterasi melalui refleksi yang telah diurutkan
                                  $.each(res.refleksi, function(index, refleksi) {
                                      var tanggal_refleksi = new Date(refleksi.tanggal);
                                      var tanggal_refleksi_str = tanggalsaja(tanggal_refleksi);
                                      var card = `
                                                <div class="col-md-2 mb-4">
                                                    <div class="card">
                                                        <div class="card-body" style="text-align: center">
                                                            <h5>Refleksi</h5>
                                                            <h6>${tanggal_refleksi_str}</h6>
                                                        </div>
                                                        <div class="card-footer" style="text-align: center">
                                                            <button class="btn btn-primary detail" data-id="${refleksi.id}">Lihat</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            `;
                                      $('#listrefleksi').append(
                                          card); // Menambahkan card ke dalam div
                                  });
                              } else {
                                  $('#listrefleksi').html(
                                      '<h6  style="text-align: center">Belum Ada Refleksi Pada Pelatihan Ini</h6>'
                                  );
                              }

                          } else {
                              Swal.fire({
                                  icon: 'error',
                                  title: 'Pelatihan Tidak Ditemukan',
                                  text: 'Periksa Kembali Token!',
                              });
                          }


                      },
                      error: function(error) {
                          togglePreloader(false)
                          Swal.fire({
                              icon: 'error',
                              title: 'Tidak Dapat Mengakses Data',
                              text: 'Coba Kembali Atau Refresh Halaman!',
                          });
                          // Handle error
                          console.log(error);
                      }
                  });
              }
              $('#formtoken').submit(function(event) {
                  event.preventDefault(); // Mencegah aksi default form
                  // Lakukan sesuatu di sini, misalnya validasi atau pengiriman data dengan AJAX
              });
              $('#formtambah').submit(function(event) {
                  event.preventDefault(); // Mencegah aksi default form
                  // Lakukan sesuatu di sini, misalnya validasi atau pengiriman data dengan AJAX
              });

              function togglePreloader(show) {
                  if (show) {
                      $('.overlay').addClass('show-overlay');
                      $('.preloader').addClass('show-preloader');
                  } else {
                      $('.overlay').removeClass('show-overlay');
                      $('.preloader').removeClass('show-preloader');
                  }
              }

              function tanggalsaja(date) {
                  var day = String(date.getDate()).padStart(2, '0');
                  var month = String(date.getMonth() + 1).padStart(2, '0');
                  var year = date.getFullYear();

                  return day + '-' + month + '-' + year;
              }
              $('#tanggal').on('input', function() {
                  var nilaiInput = $(this).val();
                  var tanggal = formatTanggal(nilaiInput);
                  $(this).val(tanggal);
              });

              function formatTanggal(input) {
                  // Hapus karakter selain angka
                  var cleaned = input.replace(/\D/g, '');

                  // Format menjadi dd-mm-yyyy
                  var formatted = '';
                  if (cleaned.length > 0) {
                      formatted = cleaned.match(/^(\d{1,2})(\d{0,2})?(\d{0,4})?/).slice(1).filter(Boolean).join('-');
                  }
                  return formatted;
              }
          })
      </script>
  @endsection
