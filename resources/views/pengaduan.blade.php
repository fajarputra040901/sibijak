  @extends('master.inner')

  @section('content')
      <section class="inner-page">
          <div class="container">
              <div class="card">
                  <div class="card-body">
                      <h3 style="text-align: center">Layanan Pengaduan Masyarakat</h3>
                      <br>
                      <div class="card">
                          <div class="card-body">
                              <p>Layanan Pengaduan Mayarakat merupakan sistem yang disediakan oleh Upelkes Jabar bagi Anda
                                  yang memiliki saran dan keluhan sebagai bentuk pengawasan terhadap tata kelola
                                  pemerintahan dan kinerja di lingkungan Upelkes Jabar.</p>
                              <p>Pengaduan Anda akan mudah ditindaklanjuti apabila memenuhi kriteria sebagai berikut:</p>
                              <ul>
                                  <li>Apa substansi pengaduan / penyimpangan kasus yang dilaporkan?</li>
                                  <li>Dimana kasus tersebut dilakukan?</li>
                                  <li>Kapan kasus tersebut dilakukan?</li>
                                  <li>Siapa Pegawai yang terlibat ?</li>
                                  <li>Kenapa penyimpangan itu dapat dilakukan?</li>
                                  <li>Bagaimana kronologis penyimpangan itu dilakukan?</li>
                              </ul>
                              <p><i><b>Notes : Upelkes Jabar akan merahasiakan identitas diri Anda sebagai Pelapor dan tidak
                                          akan berdampak pada apapun. Fokus kami kepada materi informasi yang Anda
                                          laporkan.</b></i></p>
                          </div>
                      </div>
                      <div class="card mt-3">
                          <div class="card-body">
                              <h5>Cek Status Pengaduanmu</h5>
                              <form action="/cek_status" method="get">
                                  <div class="mb-3 row">
                                      <label for="tiket" class="col-sm-2 col-form-label">Tiket</label>
                                      <div class="col-sm-6">
                                          <input type="text" class="form-control" id="tiket" name="tiket"
                                              placeholder="Masukan Nomor Tiketmu Disini">
                                      </div>
                                      <div class="col-sm-3">
                                          <button class="btn btn-primary">Cek</button>
                                      </div>

                                  </div>
                              </form>

                          </div>

                      </div>

                      <div class="card mt-3">
                          <div class="card-body">
                              <h4 style="text-align: center" class="mb-4">Formulir Pengaduan Masyarakat</h4>

                              <h5 style="text-align: center;text-decoration: underline;" class="mb-4">Identitas</h5>
                              <form action="{{ route('pengaduan_store') }}" method="POST" enctype="multipart/form-data">
                                  @csrf
                                  <input type="text" hidden name="jenis" value="P">
                                  <div class="mb-3 row">
                                      <label for="nama" class="col-sm-2 col-form-label">Nama Lengkap</label>
                                      <div class="col-sm-8">
                                          <input required type="text" class="form-control" id="nama" name="nama">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="tgllahir" class="col-sm-2 col-form-label">Tanggal Lahir
                                      </label>
                                      <div class="col-sm-8">
                                          <input type="text" autocomplete="off" data-toggle="datepicker"
                                              class="form-control" id="tgllahir" name="tgllahir" placeholder="DD-MM-YYYY">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="" class="col-sm-2 col-form-label">Jenis
                                          Kelamin</label>
                                      <div class="col-sm-8">
                                          <div class="form-check">
                                              <input class="form-check-input" type="radio" value="L" name="jk"
                                                  id="laki">
                                              <label class="form-check-label" for="laki">
                                                  Laki Laki
                                              </label>
                                          </div>
                                          <div class="form-check">
                                              <input class="form-check-input" type="radio" value="P" name="jk"
                                                  id="perempuan">
                                              <label class="form-check-label" for="perempuan">
                                                  Perempuan
                                              </label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="nohp" class="col-sm-2 col-form-label">No Telp</label>
                                      <div class="col-sm-8">
                                          <input type="text" class="form-control" name="nohp" id="nohp">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="email" class="col-sm-2 col-form-label">Email</label>
                                      <div class="col-sm-8">
                                          <input type="email" class="form-control" name="email" id="email">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="pekerjaan" class="col-sm-2 col-form-label">Pekerjaan</label>
                                      <div class="col-sm-8">
                                          <input type="text" class="form-control" name="pekerjaan" id="pekerjaan">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="instansi" class="col-sm-2 col-form-label">Instansi / Perusahaan</label>
                                      <div class="col-sm-8">
                                          <input type="text" class="form-control" name="instansi" id="instansi">
                                      </div>
                                  </div>
                                  <h5 style="text-align: center;text-decoration: underline;" class="mb-4">Laporan</h5>
                                  <div class="mb-3 row">
                                      <label for="tanggal" class="col-sm-2 col-form-label">Tanggal Kejadian
                                      </label>
                                      <div class="col-sm-8">
                                          <input type="text" autocomplete="off" data-toggle="datepicker"
                                              class="form-control" id="tanggal" name="tanggal"
                                              placeholder="DD-MM-YYYY">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="uraian" class="col-sm-2 col-form-label">Uraian Pengaduan</label>
                                      <div class="col-sm-8">
                                          <textarea name="uraian" id="uraian" class="form-control" cols="50"></textarea>
                                          <div class="valid-feedback" style="display: block">
                                              Uraikan Pengaduan Sejelas dan Sedetail Mungkin Serta Mencakup 5W1H
                                          </div>
                                      </div>
                                  </div>

                                  <h4>Upload Bukti</h4>
                                  <div class="droppable-area" id="droppableArea">
                                      Drag & Drop file di sini atau klik untuk memilih file.
                                      <input type="file" id="fileInput" name="bukti[]" multiple
                                          style="display: none;">
                                  </div>

                                  <div id="fileTable" style="display: none;">
                                      <table class="table table-bordered">
                                          <tbody id="fileList">
                                          </tbody>
                                      </table>
                                  </div>
                                  <div class="valid-feedback" style="display: block">
                                      Upload file Dapat Berupa Gambar (JPG,JPEG,PNG,HEIC), Dokumen (PDF,DOCX,XLSX,PPTX),
                                      Video (MP4,MKV,MOV), Rekaman Suara (OGG,MP3)
                                  </div>
                                  <div class="form-check mt-3">
                                      <input required class="form-check-input" type="checkbox" value=""
                                          id="flexCheckDefault">
                                      <label class="form-check-label" for="flexCheckDefault">
                                          Dengan ini menyatakan bahwa pengaduan ini adalah kejadian/kesaksian yang
                                          sebenarnya
                                          terjadi. Saya akan mematuhi ketentuan yang berlaku.
                                      </label>
                                  </div>
                                  <button class="btn btn-primary mt-4">Kirim Pengaduan</button>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <script src="{{ asset('date/js/datepicker.js') }}"></script>
      {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> --}}
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

      <script>
          // Ambil elemen yang dibutuhkan
          const droppableArea = document.getElementById('droppableArea');
          const fileInput = document.getElementById('fileInput');
          const fileList = document.getElementById('fileList');
          const uploadButton = document.getElementById('uploadButton');
          const fileTable = document.getElementById('fileTable');

          // Fungsi untuk menambahkan file ke daftar
          function addFileToList(file) {
              const fileItem = document.createElement('tr');

              const fileNameCell = document.createElement('td');
              fileNameCell.textContent = file.name;

              const fileActionCell = document.createElement('td');
              const fileDeleteButton = document.createElement('button');
              fileDeleteButton.textContent = 'Hapus';
              fileDeleteButton.classList.add('btn', 'btn-sm', 'btn-danger');
              fileDeleteButton.addEventListener('click', () => {
                  fileItem.remove();
                  if (fileList.children.length === 0) {
                      fileTable.style.display = 'none';
                  }
              });

              fileActionCell.appendChild(fileDeleteButton);

              fileItem.appendChild(fileNameCell);
              fileItem.appendChild(fileActionCell);

              fileList.appendChild(fileItem);
              fileTable.style.display = 'block';
          }

          // Tambahkan event listener untuk drag & drop
          droppableArea.addEventListener('dragover', (e) => {
              e.preventDefault();
              droppableArea.classList.add('active');
          });

          droppableArea.addEventListener('dragleave', () => {
              droppableArea.classList.remove('active');
          });

          droppableArea.addEventListener('drop', (e) => {
              e.preventDefault();
              droppableArea.classList.remove('active');

              const files = e.dataTransfer.files;
              Array.from(files).forEach(file => addFileToList(file));
          });

          // Tambahkan event listener untuk klik pada area droppable
          droppableArea.addEventListener('click', () => {
              fileInput.click();
          });

          // Tambahkan event listener untuk memilih file dari input
          fileInput.addEventListener('change', () => {
              const files = fileInput.files;
              Array.from(files).forEach(file => addFileToList(file));
          });

          // Tambahkan event listener untuk tombol Upload
      </script>
      <script>
          $(document).ready(function() {
              $('[data-toggle="datepicker"]').datepicker({
                  autoHide: true,
                  zIndex: 2048,
                  format: 'dd-mm-yyyy',
              });
              $('#tgllahir').on('input', function() {
                  var nilaiInput = $(this).val();
                  var tanggal = formatTanggal(nilaiInput);
                  $(this).val(tanggal);
              });

              function formatTanggal(input) {
                  // Hapus karakter selain angka
                  var cleaned = input.replace(/\D/g, '');

                  // Format menjadi dd-mm-yyyy
                  var formatted = '';
                  if (cleaned.length > 0) {
                      formatted = cleaned.match(/^(\d{1,2})(\d{0,2})?(\d{0,4})?/).slice(1).filter(Boolean).join('-');
                  }
                  return formatted;
              }
          })
      </script>
  @endsection
