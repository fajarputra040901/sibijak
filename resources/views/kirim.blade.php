  @extends('master.inner')

  @section('content')
      <section class="inner-page">
          <div class="container">
              <div class="card">
                  <div class="card-body">
                      <h3 style="text-align: center">Pengaduan Berhasil Dikirim</h3>
                      <br>
                      <div class="card">
                          <div class="card-body">

                              <div class="card" style="text-align: center">
                                  <div class="card-body">
                                      <h5>Nomor Tiket Pengaduan</h5>
                                      <br>
                                      <h5><b> {{ $data->token }} </b></h5>

                                      <br>
                                      <p><i><b>Simpan Kode Tiket Ini Untuk Melakukan Pelacakan Pengaduan Anda</b></i></p>

                                  </div>
                              </div>
                              <div class="card mt-3">
                                  <div class="card-body">
                                      <h5 style="text-align: center">Status Pengaduan</h5>
                                      <div class="table-responsive mt-3">
                                          <table class="table">
                                              <tr>
                                                  <th>Waktu</th>
                                                  <th>Status</th>
                                                  <th>Keterangan</th>
                                              </tr>

                                              <tr id="trpengaduan" style="display: none">
                                                  <td><span id="waktu_pengaduan"></span></td>
                                                  <td><span id="status_pengaduan"></span></td>
                                                  <td><span id="ket_pengaduan"></span></td>
                                              </tr>
                                              <tr id="trverifikasi" style="display: none">
                                                  <td><span id="waktu_verifikasi_status"></span></td>
                                                  <td><span id="verifikasi_status"></span></td>
                                                  <td id="ket_verifikasi_status"></td>

                                              </tr>
                                              <tr id="trka" style="display: none">
                                                  <td><span id="waktu_ka_status"></span></td>
                                                  <td><span id="ka_status"></span></td>
                                                  <td id="ket_ka_status"></td>
                                              </tr>
                                              <tr id="trbagian" style="display: none">
                                                  <td><span id="waktu_bagian_status"></span></td>
                                                  <td><span id="bagian_status"></span></td>
                                                  <td id="ket_bagian_status"></td>
                                              </tr>
                                              <tr id="trselesai" style="display: none">
                                                  <td><span id="waktu_selesai_status"></span></td>
                                                  <td><span id="selesai_status"></span></td>
                                                  <td><span id="ket_selesai_status"></span></td>
                                              </tr>

                                          </table>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>



                  </div>
              </div>
          </div>
      </section>
      <script>
          $(document).ready(function() {
              table({{ $data->id }})

              function table(id) {
                  $.ajax({
                      url: '/status_track/' + id,
                      type: 'GET',
                      dataType: 'json',
                      success: function(res) {
                          $("#trpengaduan").attr("style", "display:null");
                          var time_pengaduan = new Date(res.created_at);
                          var tanggal_pengaduan = formatDate(time_pengaduan);
                          $('#waktu_pengaduan').text(tanggal_pengaduan);
                          $('#status_pengaduan').text("Pengaduan Baru");
                          $('#ket_pengaduan').text("Sedang Di Proses");

                          if (res.status !== null) {
                              if (res.status !== "Pengaduan Baru") {
                                  $('#ket_pengaduan').text("Sedang Di Tanggapi");
                                  $("#trverifikasi").attr("style", "display:null");
                                  var time_verifikasi = new Date(res.waktu_verifikasi);
                                  var tanggal_verifikasi = formatDate(time_verifikasi);
                                  $('#waktu_verifikasi_status').text(tanggal_verifikasi);
                                  $('#verifikasi_status').text(res.status);
                                  $('#ket_verifikasi_status').html('<span>' + res.respon_verifikasi +
                                      '</span>' +
                                      '<br><span><i>Dilakukan Oleh Verifikator</i></span>');
                              }

                          }
                          if (res.status_ka !== null) {
                              $("#trka").attr("style", "display:null");
                              var time_ka = new Date(res.waktu_ka);
                              var tanggal_ka = formatDate(time_ka);
                              $('#waktu_ka_status').text(tanggal_ka);
                              $('#ka_status').text(res.status_ka);
                              $('#ket_ka_status').html('<span>' + res.respon_ka + '</span>' +
                                  '<br><span><i>Dilakukan Oleh KA Sub Bagian</i></span>');
                          }
                          if (res.status_bagian !== null) {
                              $("#trbagian").attr("style", "display:null");
                              var time_bagian = new Date(res.waktu_pemberian_bagian);
                              var tanggal_bagian = formatDate(time_bagian);
                              $('#waktu_bagian_status').text(tanggal_bagian);
                              $('#bagian_status').text(res.status_pemberian_bagian);
                              $('#ket_bagian_status').html(
                                  '<span>KA Sub Bag Sudah Melakukan Penugasan Kepada Bagian ' + res
                                  .bagian + '</span>');
                          }
                          if (res.status_bagian !== null) {
                              $("#trbagian").attr("style", "display:null");
                              var time_bagian = new Date(res.waktu_bagian);
                              var tanggal_bagian = formatDate(time_bagian);
                              $('#waktu_bagian_status').text(tanggal_bagian);
                              $('#bagian_status').text(res.status_bagian);
                              $('#ket_bagian_status').html('<span>' + res.ket_bagian + '</span>' +
                                  '<br><span><i>Dilakukan Oleh Bagian ' + res
                                  .bagian + '</i></span>');
                          }
                          if (res.status_selesai !== null) {
                              $("#trselesai").attr("style", "display:null");
                              var time_selesai = new Date(res.waktu_selesai);
                              var tanggal_selesai = formatDate(time_selesai);
                              $('#waktu_selesai_status').text(tanggal_selesai);
                              $('#selesai_status').text(res.status_selesai);
                              $('#ket_selesai_status').text(res.ket_selesai);
                          }






                      },
                      error: function(xhr, status, error) {
                          console.error(xhr.responseText); // Menampilkan pesan kesalahan dalam konsol
                      }
                  });
              }

              function formatDate(date) {
                  var day = String(date.getDate()).padStart(2, '0');
                  var month = String(date.getMonth() + 1).padStart(2, '0'); // January is 0!
                  var year = date.getFullYear();
                  var hours = String(date.getHours()).padStart(2, '0');
                  var minutes = String(date.getMinutes()).padStart(2, '0');

                  return day + '-' + month + '-' + year + ' (' + hours + ':' + minutes + ')';
              }

          });
      </script>
  @endsection
