@extends('master.master')

@section('content')
    <div class="row">
        <div class="col-xl-4 col-sm-6">
            <div class="card info-card revenue-card">



                <div class="card-body">
                    <h5 class="card-title">Jumlah <span>| Pelatihan</span></h5>

                    <div class="d-flex align-items-center">
                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                            <i class="bi bi-blockquote-right"></i>
                        </div>
                        <div class="ps-3">
                            <h6 id="jumlah"></h6>


                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-xl-4 col-sm-6">
            <div class="card info-card sales-card" id="pencarian" style="cursor: pointer">



                <div class="card-body">
                    <h5 class="card-title">Tambah </h5>

                    <div class="d-flex align-items-center">
                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                            <i class="bi bi-search"></i>
                        </div>
                        <div class="ps-3">
                            <h6>Tambah Pelatihan</h6>
                            {{-- <span class="text-success small pt-1 fw-bold">12%</span> <span
                                  class="text-muted small pt-2 ps-1">increase</span> --}}

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-xl-4 col-sm-6">
            <div class="card info-card revenue-card" id="reload" style="cursor: pointer">



                <div class="card-body">
                    <h5 class="card-title">Reload Data </h5>

                    <div class="d-flex align-items-center">
                        <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                            <i class="bi bi-arrow-repeat"></i>
                        </div>
                        <div class="ps-3">
                            <h6>Reload</h6>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="card mt-3">
        <div class="card-body ">
            <h5 class="card-title">Data Pelatihan </h5>
            {{-- <div class="nav-align-top">
                  <ul class="nav nav-tabs" role="tablist">
                      <li class="nav-item">
                          <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab"
                              data-bs-target="#navs-top-home" aria-controls="navs-top-home"
                              aria-selected="true">Tunggal</button>
                      </li>
                      <li class="nav-item">
                          <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                              data-bs-target="#navs-top-profile" aria-controls="navs-top-profile"
                              aria-selected="false">Olahan</button>
                      </li>

                  </ul>
                  <div class="tab-content">
                      <div class="tab-pane fade show active table-responsive" id="navs-top-home" role="tabpanel">
                          <table class="table" id="tunggal">

                          </table>
                      </div>
                      <div class="tab-pane fade table-responsive" id="navs-top-profile" role="tabpanel">

                      </div>

                  </div>
              </div> --}}
            <div class="row">
                <div class="col-lg-12">
                    <table class="table " id="table">
                        <thead>
                            <th>No</th>
                            <th style="width:25%">Tanggal Pelatihan</th>
                            <th>Nama Pelatihan</th>
                            <th>MOT</th>
                            <th>Token</th>
                            <th>Aksi</th>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="modaldetail" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="modaltambahLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modaltambahLabel">Detail Pelatihan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="mt-3" style="text-align: center">Data Pelatihan</h5>
                            <form action="" id="formpencarian">
                                <input type="text" id="id_pelatihan" hidden>
                                <div class="mb-3 row">
                                    <label class="col-sm-2 col-form-label">Tanggal Pelatihan
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" autocomplete="off" data-toggle="datepicker"
                                            class="form-control" id="tanggalawal_hasil" name="tanggalawal_hasil"
                                            placeholder="DD-MM-YYYY">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" autocomplete="off" data-toggle="datepicker"
                                            class="form-control" id="tanggalakhir_hasil" name="tanggalakhir_hasil"
                                            placeholder="DD-MM-YYYY">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="token_hasil" class="col-sm-2 col-form-label">Token</label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control" name="token_hasil"
                                            id="token_hasil">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="nama_pelatihan_hasil" class="col-sm-2 col-form-label">Nama Pelatihan</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="nama_pelatihan_hasil"
                                            id="nama_pelatihan_hasil">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="mot_hasil" class="col-sm-2 col-form-label">Nama Pelatihan</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="mot_hasil" id="mot_hasil">
                                    </div>
                                </div>



                                <button type="button" class="btn btn-primary" id="update">Update</button>
                            </form>
                        </div>
                    </div>
                    <div class="card mt-3">
                        <div class="card-body">
                            <div class="mt-3">

                                <h5>Hapus Pelatihan</h5>
                                <button class="btn btn-danger" id="hapus_pelatihan">Hapus</button>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>



    <div class="modal fade" id="modalpencarian" tabindex="-1" aria-labelledby="fileModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="fileModalLabel">Tambah Pelatihan</h5>
                    <button type="button" class="btn-close tutup_modal" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form action="" id="formpencarian">
                                <div class="mb-3 row">
                                    <label class="col-sm-2 col-form-label">Tanggal Pelatihan
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" autocomplete="off" data-toggle="datepicker"
                                            class="form-control" id="tanggalawal" name="tanggalawal"
                                            placeholder="DD-MM-YYYY">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" autocomplete="off" data-toggle="datepicker"
                                            class="form-control" id="tanggalakhir" name="tanggalakhir"
                                            placeholder="DD-MM-YYYY">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="nama_pelatihan" class="col-sm-2 col-form-label">Nama Pelatihan</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="nama_pelatihan"
                                            id="nama_pelatihan">
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="mot" class="col-sm-2 col-form-label">MOT</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="mot" id="mot">
                                    </div>
                                </div>
                                <button type="button" class="btn btn-primary " id="tampilkan">Submit</button>
                            </form>



                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary tutup_modal" data-bs-dismiss="modal">Close</button>

                </div>

            </div>
        </div>
    </div>


    <script src="{{ asset('date/js/datepicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            jumlah();

            var table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: '/list_pelatihan',

                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0 // Kolom nomor
                }],
                "order": [
                    [0, 'asc']
                ],
                columns: [{
                        data: null,
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'tanggal'
                    },
                    {
                        data: 'nama_pelatihan'
                    },
                    {
                        data: 'mot'
                    },

                    {
                        data: 'token'
                    },

                    {
                        data: 'tombol',
                        name: 'tombol',
                        orderable: false,
                        searchable: false,
                    },
                ]
            });
            $('.tutup_modal').click(function() {

                $('#formpencarian')[0].reset();
            });
            $('[data-toggle="datepicker"]').datepicker({
                autoHide: true,
                zIndex: 2048,
                format: 'dd-mm-yyyy',
            });

            $('#tampilkan').click(function() {
                lakukanPencarian();
                $('#modalpencarian').modal('hide');
            });
            $('#update').click(function() {
                update();

            });
            $('#hapus_pelatihan').click(function() {
                hapus();

            });
            $('#pencarian').click(function() {
                $('#modalpencarian').modal('show');
            });








            $('#table').on('click', '.detail', function() {
                var id = $(this).data('id');
                detail(id);
                $('#modaldetail').modal('show');

            });





            $('#reload').click(function() {
                jumlah();

                table.ajax.reload();
            });




            $('#formpencarian').submit(function(event) {
                event.preventDefault(); // Mencegah aksi default form
                // Lakukan sesuatu di sini, misalnya validasi atau pengiriman data dengan AJAX
            });
            $('#formupdate').submit(function(event) {
                event.preventDefault(); // Mencegah aksi default form
                // Lakukan sesuatu di sini, misalnya validasi atau pengiriman data dengan AJAX
            });

            function detail(id) {
                $.ajax({
                    url: '/detail_pelatihan/' + id,
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {

                        var tanggal_a = new Date(data.tanggal_mulai);
                        var tanggal_mulai = tanggalsaja(tanggal_a);
                        var tanggal_m = new Date(data.tanggal_berakhir);
                        var tanggal_berakhir = tanggalsaja(tanggal_m);

                        $('#id_pelatihan').val(data.id);
                        $('#token_hasil').val(data.token);
                        $('#mot_hasil').val(data.mot);
                        $('#nama_pelatihan_hasil').val(data.nama_pelatihan);
                        $('#tanggalawal_hasil').val(tanggal_mulai);
                        $('#tanggalakhir_hasil').val(tanggal_berakhir);

                    },
                    error: function(xhr, status, error) {
                        console.error(xhr.responseText);
                    }
                });
            }

            function update() {
                var id = $('#id_pelatihan').val();
                var tanggalawal = $('#tanggalawal_hasil').val();
                var tanggalakhir = $('#tanggalakhir_hasil').val();
                var nama_pelatihan = $('#nama_pelatihan_hasil').val();
                var mot = $('#mot_hasil').val();

                // Memeriksa apakah setidaknya satu input diisi
                if (tanggalawal || tanggalakhir || nama_pelatihan || mot) {
                    Swal.fire({
                        title: 'Konfirmasi?',
                        text: 'Apakah Anda Yakin!',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya!',
                        cancelButtonText: 'Batal'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            // Mendapatkan CSRF token dari meta tag
                            var csrfToken = $('meta[name="csrf-token"]').attr('content');

                            // Mengirim data menggunakan AJAX
                            $.ajax({
                                type: 'post', // Metode HTTP yang digunakan
                                url: '/update_pelatihan', // URL tujuan
                                data: {
                                    'id': id,
                                    'tanggalawal': tanggalawal,
                                    'tanggalakhir': tanggalakhir,
                                    'nama_pelatihan': nama_pelatihan,
                                    'mot': mot,
                                }, // Data yang dikirim
                                headers: {
                                    'X-CSRF-TOKEN': csrfToken // Menggunakan CSRF token
                                },
                                dataType: 'json', // Tipe data yang diharapkan dari server
                                success: function(response) {
                                    table.ajax.reload();
                                    jumlah();
                                    // Menampilkan sweetalert2 jika berhasil
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Sukses',
                                        text: 'Pelatihan berhasil Diupdate',
                                    });
                                },
                                error: function(error) {
                                    // Menampilkan sweetalert2 jika terjadi error
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Terjadi kesalahan. Silakan coba lagi!',
                                    });
                                }
                            });
                        }
                    });
                } else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Data Tidak Lengkap',
                        text: 'Lengkapi Data yang Akan Di Update',
                    });
                }
            }




            function hapus() {
                var id = $('#id_pelatihan').val();

                // Memeriksa apakah setidaknya satu input diisi
                if (id) {
                    Swal.fire({
                        title: 'Konfirmasi?',
                        text: 'Apakah Anda Yakin!',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, hapus!',
                        cancelButtonText: 'Batal'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            var dataParams = {
                                'id': id,
                            };

                            // Mendapatkan CSRF token dari meta tag
                            var csrfToken = $('meta[name="csrf-token"]').attr('content');

                            // Mengirim data menggunakan AJAX
                            $.ajax({
                                type: 'POST', // Metode HTTP yang digunakan
                                url: '/hapus_pelatihan', // URL tujuan
                                data: dataParams, // Data yang dikirim
                                headers: {
                                    'X-CSRF-TOKEN': csrfToken // Menggunakan CSRF token
                                },
                                dataType: 'json', // Tipe data yang diharapkan dari server
                                success: function(response) {
                                    table.ajax.reload();
                                    jumlah();
                                    $('#modaldetail').modal('hide');
                                    // Menampilkan sweetalert2 jika berhasil
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Sukses',
                                        text: 'Pelatihan berhasil Dihapus',
                                    });
                                },
                                error: function(error) {
                                    // Menampilkan sweetalert2 jika terjadi error
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Terjadi kesalahan. Silakan coba lagi!',
                                    });
                                }
                            });
                        }
                    });
                } else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Data Tidak Lengkap',
                        text: 'Lengkapi Data yang Akan Di Hapus',
                    });
                }
            }








            function formatDate(date) {
                var day = String(date.getDate()).padStart(2, '0');
                var month = String(date.getMonth() + 1).padStart(2, '0'); // January is 0!
                var year = date.getFullYear();
                var hours = String(date.getHours()).padStart(2, '0');
                var minutes = String(date.getMinutes()).padStart(2, '0');

                return day + '-' + month + '-' + year + ' (' + hours + ':' + minutes + ')';
            }

            function tanggalsaja(date) {
                var day = String(date.getDate()).padStart(2, '0');
                var month = String(date.getMonth() + 1).padStart(2, '0');
                var year = date.getFullYear();

                return day + '-' + month + '-' + year;
            }

            function jumlah() {
                $.ajax({
                    url: '/jumlah_pelatihan',
                    type: 'GET',
                    dataType: 'json',
                    success: function(res) {

                        $('#jumlah').text(res.jumlah);



                    },
                    error: function(xhr, status, error) {
                        console.error(xhr.responseText); // Menampilkan pesan kesalahan dalam konsol
                    }
                });
            }












            function lakukanPencarian() {
                var tanggalawal = $('#tanggalawal').val();
                var tanggalakhir = $('#tanggalakhir').val();
                var nama_pelatihan = $('#nama_pelatihan').val();
                var mot = $('#mot').val();

                // Memeriksa apakah setidaknya satu input diisi
                if (tanggalawal || tanggalakhir || nama_pelatihan || mot) {

                    // Mendapatkan CSRF token dari meta tag
                    var csrfToken = $('meta[name="csrf-token"]').attr('content');

                    // Mengirim data menggunakan AJAX
                    $.ajax({
                        type: "POST",
                        url: "/tambah_pelatihan",
                        data: {
                            _token: csrfToken,
                            tanggalawal: tanggalawal,
                            tanggalakhir: tanggalakhir,
                            nama_pelatihan: nama_pelatihan,
                            mot: mot,
                        },
                        success: function(response) {
                            table.ajax.reload();
                            $('#modalpencarian').modal('hide');
                            $('#formpencarian')[0].reset();
                            // Menampilkan sweetalert2 jika berhasil
                            Swal.fire({
                                icon: 'success',
                                title: 'Sukses',
                                text: 'Pelatihan berhasil ditambahkan',
                            });

                        },
                        error: function(xhr, status, error) {
                            console.error(xhr.responseText);
                        }
                    });
                } else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Data Tidak Lengkap',
                        text: 'Lengkapi Data yang Akan Di Input',
                    });
                }
            }

            function togglePreloader(show) {
                if (show) {
                    $('.overlay').addClass('show-overlay');
                    $('.preloader').addClass('show-preloader');
                } else {
                    $('.overlay').removeClass('show-overlay');
                    $('.preloader').removeClass('show-preloader');
                }
            }

        });
    </script>
@endsection
