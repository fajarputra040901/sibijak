  @extends('master.master')

  @section('content')
      <div class="row">
          <div class="col-xl-4 col-sm-6">
              <div class="card info-card revenue-card">



                  <div class="card-body">
                      <h5 class="card-title">Jumlah <span>| Pengaduan</span></h5>

                      <div class="d-flex align-items-center">
                          <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                              <i class="bi bi-blockquote-right"></i>
                          </div>
                          <div class="ps-3">
                              <h6 id="jumlah"></h6>


                          </div>
                      </div>
                  </div>

              </div>
          </div>
          <div class="col-xl-4 col-sm-6">
              <div class="card info-card sales-card" id="pencarian" style="cursor: pointer">



                  <div class="card-body">
                      <h5 class="card-title">Pencarian </h5>

                      <div class="d-flex align-items-center">
                          <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                              <i class="bi bi-search"></i>
                          </div>
                          <div class="ps-3">
                              <h6>Filter / Pencarian</h6>
                              {{-- <span class="text-success small pt-1 fw-bold">12%</span> <span
                                  class="text-muted small pt-2 ps-1">increase</span> --}}

                          </div>
                      </div>
                  </div>

              </div>
          </div>
          <div class="col-xl-4 col-sm-6">
              <div class="card info-card revenue-card" id="reload" style="cursor: pointer">



                  <div class="card-body">
                      <h5 class="card-title">Reload Data </h5>

                      <div class="d-flex align-items-center">
                          <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                              <i class="bi bi-arrow-repeat"></i>
                          </div>
                          <div class="ps-3">
                              <h6>Reload</h6>

                          </div>
                      </div>
                  </div>

              </div>
          </div>

      </div>
      <div class="card mt-3">
          <div class="card-body ">
              <h5 class="card-title">Data Pengaduan <i class="bi-info-circle" data-bs-toggle="tooltip"
                      data-bs-placement="right"
                      title="Data Pengaduan Yang Ditampilkan Adalah Data Pengaduan Yang Masuk Hari Ini">
                  </i> </h5>
              {{-- <div class="nav-align-top">
                  <ul class="nav nav-tabs" role="tablist">
                      <li class="nav-item">
                          <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab"
                              data-bs-target="#navs-top-home" aria-controls="navs-top-home"
                              aria-selected="true">Tunggal</button>
                      </li>
                      <li class="nav-item">
                          <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                              data-bs-target="#navs-top-profile" aria-controls="navs-top-profile"
                              aria-selected="false">Olahan</button>
                      </li>

                  </ul>
                  <div class="tab-content">
                      <div class="tab-pane fade show active table-responsive" id="navs-top-home" role="tabpanel">
                          <table class="table" id="tunggal">

                          </table>
                      </div>
                      <div class="tab-pane fade table-responsive" id="navs-top-profile" role="tabpanel">

                      </div>

                  </div>
              </div> --}}
              <div class="row">
                  <div class="col-lg-12">
                      <table class="table " id="table">
                          <thead>
                              <th>No</th>
                              <th width="20%">Tanggal Pengaduan</th>
                              <th>Nama</th>

                              <th>Tiket</th>
                              <th width="30%">Status</th>
                              <th>Aksi</th>
                          </thead>
                      </table>
                  </div>
              </div>

          </div>
      </div>

      <div class="modal fade" id="modaldetail" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
          aria-labelledby="modaltambahLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable modal-xl">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="modaltambahLabel">Detail Pengaduan</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                      <div class="card">
                          <div class="card-body">
                              <h4 style="text-align: center">Identitas</h4>
                              <table class="table">
                                  <tr>
                                      <td>Tanggal Pengaduan</td>
                                      <td>: <span id="tanggalpengaduandetail"></span></td>
                                  </tr>
                                  <tr>
                                      <td>Nama</td>
                                      <td>: <span id="namadetail"></span></td>
                                  </tr>
                                  <tr>
                                      <td>Tanggal Lahir</td>
                                      <td>: <span id="tgllahirdetail"></span></td>
                                  </tr>
                                  <tr>
                                      <td>Jenis Kelamin</td>
                                      <td>: <span id="jkdetail"></span></td>
                                  </tr>
                                  <tr>
                                      <td>No Telp</td>
                                      <td>: <span id="nohpdetail"></span></td>
                                  </tr>
                                  <tr>
                                      <td>Email</td>
                                      <td>: <span id="emaildetail"></span></td>
                                  </tr>
                                  <tr>
                                      <td>Pekerjaan</td>
                                      <td>: <span id="pekerjaandetail"></span></td>
                                  </tr>
                                  <tr>
                                      <td>Instansi</td>
                                      <td>: <span id="instansidetail"></span></td>
                                  </tr>

                              </table>
                          </div>
                      </div>
                      <div class="card mt-3">
                          <div class="card-body">
                              <h4 style="text-align: center">Laporan</h4>
                              <table class="table">
                                  <tr>
                                      <td>Tanggal Kejadian</td>
                                      <td>: <span id="tanggaldetail"></span></td>
                                  </tr>
                                  <tr>
                                      <td>Uraian</td>
                                      <td>
                                          <textarea class="form-control" id="uraiandetail" readonly rows="6"></textarea>
                                      </td>
                                  </tr>
                              </table>
                              <h5 style="text-align: center">Bukti</h5>
                              <table class="table" id="tableBukti"></table>
                          </div>
                      </div>

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  </div>

              </div>
          </div>
      </div>

      <div class="modal fade" id="modalproses" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
          aria-labelledby="modaltambahLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable modal-xl">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="modaltambahLabel">Proses Pengaduan</h5>
                      <button type="button" class="btn-close" id="tutup" data-bs-dismiss="modal"
                          aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                      <input type="text" hidden id="idverifikator">
                      <div class="nav-align-top">
                          <ul class="nav nav-tabs" role="tablist">
                              <li class="nav-item">
                                  <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab"
                                      data-bs-target="#verifikator" id="verifikatorbtn" aria-controls="verifikator"
                                      aria-selected="true">Verifikator</button>
                              </li>
                              <li class="nav-item">
                                  <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                      data-bs-target="#disposisi" id="disposisibtn" aria-controls="disposisi"
                                      aria-selected="false">Disposisi</button>
                              </li>
                              <li class="nav-item">
                                  <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                      data-bs-target="#tindaklanjut" id="tindaklanjutbtn" aria-controls="tindaklanjut"
                                      aria-selected="false">Tindak Lanjut</button>
                              </li>
                              <li class="nav-item">
                                  <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                      data-bs-target="#penyelesaian" id="selesaibtn" aria-controls="penyelesaian"
                                      aria-selected="false">Penyelesaian</button>
                              </li>
                              <li class="nav-item">
                                  <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                      data-bs-target="#status" id="statusbtn" aria-controls="status"
                                      aria-selected="false">Status</button>
                              </li>

                          </ul>
                          <div class="tab-content">

                              <div class="tab-pane fade show active" id="verifikator" role="tabpanel">
                                  <div id="isiverifikator"style="display: none">
                                      <div class="card mt-3">
                                          <div class="card-body">
                                              <div class="mt-3">
                                                  <div id="verifikasi_selesai" style="display: none">
                                                      <p>Pengaduan Ini Sudah Dinyatakan <b>Selesai</b></p>

                                                  </div>
                                                  <div id="verifikasi_diterima" style="display: none">
                                                      <p>Pengaduan Ini Sudah <b>Diverifikasi</b>, Lakukan Update Apabila Ada
                                                          Perubahan.</p>
                                                      <p><i><b>Note : Apabila Status Ingin Dirubah Menjadi "Ditolak" Maka
                                                                  Proses
                                                                  Setelah Ini Akan Otomatis Terhapus Dan Pengaduan Akan
                                                                  Otomastis
                                                                  Selesai</b></i></p>
                                                  </div>

                                                  <div id="verifikasi_ditolak" style="display: none">
                                                      <p>Pengaduan Ini Sudah <b>Ditolak</b>, Pengaduan Dinyatakan Selesai
                                                      </p>

                                                  </div>

                                                  <div class="mb-3 row">
                                                      <label for="waktu_verifikasi" class="col-sm-2 col-form-label">Waktu
                                                          Verifikasi
                                                      </label>
                                                      <div class="col-sm-8">
                                                          <input type="text" readonly class="form-control"
                                                              id="waktu_verifikasi">
                                                      </div>
                                                  </div>
                                                  <div class="mb-3 row">
                                                      <label for="verifikasi_hasil" class="col-sm-2 col-form-label">Status
                                                          Verifikasi
                                                      </label>
                                                      <div class="col-sm-8">
                                                          <input type="text" readonly class="form-control"
                                                              id="verifikasi_hasil">
                                                      </div>
                                                  </div>

                                                  <div class="mb-3 row">
                                                      <label for="ket_verifikator_hasil"
                                                          class="col-sm-2 col-form-label">Keterangan</label>
                                                      <div class="col-sm-8">
                                                          <textarea readonly name="ket_verifikator_hasil" id="ket_verifikator_hasil" class="form-control" cols="50"></textarea>
                                                      </div>
                                                  </div>
                                              </div>


                                          </div>
                                      </div>
                                  </div>
                                  <div id="formverifikator" class="mt-3">
                                      <form id="formverif">
                                          <div class="card mt-3">
                                              <div class="card-body">
                                                  <p>Verifikasi Dilakukan Oleh Verifikator Untuk Menilai Pengaduan
                                                      Apakah
                                                      Dapat Diajukan Atau Tidak</p>
                                                  <p><i><b>Note : Uraian DIlihat Dari Detail Pengaduan</b></i></p>
                                                  <p><i><b>Note : Apabila Pengaduan Ini "Ditolak" Maka
                                                              Proses
                                                              Setelah Ini Akan Otomatis Terhapus Dan Pengaduan Akan
                                                              Otomastis
                                                              Selesai</b></i></p>
                                                  <div class="mb-3 row">
                                                      <label for="verifikasi" class="col-sm-2 col-form-label">Verifikasi
                                                      </label>
                                                      <div class="col-sm-8">
                                                          <select class="form-select" id="verifikasi">
                                                              <option value="" selected>Pilih</option>
                                                              <option value="Diverifikasi">Diverifikasi</option>
                                                              <option value="Ditolak">Ditolak</option>
                                                          </select>
                                                      </div>
                                                  </div>
                                                  <div class="mb-3 row">
                                                      <label for="ket_verifikator"
                                                          class="col-sm-2 col-form-label">Keterangan</label>
                                                      <div class="col-sm-8">
                                                          <textarea name="ket_verifikator" id="ket_verifikator" class="form-control" cols="50"></textarea>
                                                      </div>
                                                  </div>
                                                  <button class="btn btn-primary" id="submitverifikator"></button>
                                              </div>
                                          </div>
                                      </form>
                                  </div>

                              </div>
                              {{-- tab disposisi --}}
                              <div class="tab-pane fade" id="disposisi" role="tabpanel">
                                  <div id="formdisposisi">
                                      <form id="formdispo">
                                          <div class="card mt-3">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <p>Disposisi Dilakukan Oleh KA Sub Bag Setelah Diverifikasi Serta
                                                          Pengaduan Dinilai Layak
                                                          Untuk Diterima Dan Ditindak Lanjuti</p>
                                                      <p><i><b>Note : Uraian DIlihat Dari Detail Pengaduan</b></i></p>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="card mt-3" id="ditolakverifikasi" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <h5><b>Pengaduan Ditolak Oleh Verifikator</b></h5>
                                                  </div>

                                              </div>
                                          </div>
                                          <div class="card mt-3" id="belumverifikasi" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <h5><b>Pengaduan Belum Diverifikasi Oleh Verifikator</b></h5>
                                                  </div>

                                              </div>
                                          </div>
                                          <div class="card mt-3" id="sudahverifikasi" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <h5><b>Pengaduan Sudah Diverifikasi Oleh Verifikator</b></h5>
                                                  </div>

                                              </div>
                                          </div>
                                          <div class="card mt-3" id="diterimakasubbag" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <h5><b>Pengaduan Sudah Diterima Oleh KA Sub Bag</b></h5>
                                                  </div>

                                              </div>
                                          </div>
                                          <div class="card mt-3" id="ditolakkasubbag" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <h5><b>Pengaduan Ditolak Oleh KA Sub Bag</b></h5>
                                                  </div>

                                              </div>
                                          </div>
                                          <div class="card mt-3" id="pemberiansukses" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <h5><b>Pengaduan Sudah Diterima Serta Sudah Menugaskan Bagian
                                                              Terkait
                                                              Untuk
                                                              Ditindak Lanjuti</b></h5>
                                                  </div>

                                              </div>
                                          </div>
                                          <div class="card mt-3" id="pemberianditangguhkan" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <p><b>Pengaduan Ditangguhkan Sedang Menunggu Untuk Diterima Dan
                                                              Ditugaskan Ke
                                                              Bagian
                                                              Terkait Untuk
                                                              Ditindak Lanjuti</b></p>
                                                  </div>

                                              </div>
                                          </div>
                                          <div class="card mt-3" id="selesaidisposisi" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <h5><b>Pengaduan Dinyatakan Selesai</b></h5>
                                                  </div>

                                              </div>
                                          </div>
                                          <div class="card mt-3" id="pemberianhasil" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <h5>Pengalihan Pengaduan Untuk Tindak Lanjut</h5>
                                                      <div class="mb-3 row">
                                                          <label for="waktubagianhasil"
                                                              class="col-sm-2 col-form-label">Waktu Penugasan
                                                          </label>
                                                          <div class="col-sm-6">
                                                              <input type="text" readonly class="form-control"
                                                                  id="waktubagianhasil">
                                                          </div>
                                                      </div>
                                                      <div class="mb-3 row">
                                                          <label for="bagianhasil" class="col-sm-2 col-form-label">Berikan
                                                              Kepada Bagian
                                                          </label>
                                                          <div class="col-sm-6">
                                                              <input type="text" readonly class="form-control"
                                                                  id="bagianhasil">
                                                          </div>
                                                      </div>


                                                  </div>
                                              </div>
                                          </div>
                                          <div class="card mt-3" id="hasildisposisi" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">

                                                      <h5>Penerimaan Pengaduan</h5>
                                                      <div class="mb-3 row">
                                                          <label for="waktupenerimaanhasil"
                                                              class="col-sm-2 col-form-label">Waktu
                                                          </label>
                                                          <div class="col-sm-8">
                                                              <input type="text" readonly class="form-control"
                                                                  id="waktupenerimaanhasil">
                                                          </div>
                                                      </div>

                                                      <div class="mb-3 row">
                                                          <label for="penerimaanhasil"
                                                              class="col-sm-2 col-form-label">Penerimaan
                                                              Pengaduan
                                                          </label>
                                                          <div class="col-sm-8">
                                                              <input type="text" readonly class="form-control"
                                                                  id="penerimaanhasil">
                                                          </div>
                                                      </div>
                                                      <div class="mb-3 row">
                                                          <label for="ket_kasub_hasil"
                                                              class="col-sm-2 col-form-label">Keterangan</label>
                                                          <div class="col-sm-8">
                                                              <textarea name="ket_kasub_hasil" id="ket_kasub_hasil" class="form-control" cols="50"></textarea>
                                                          </div>
                                                      </div>

                                                  </div>
                                              </div>
                                          </div>
                                          <div class="card mt-3" id="pemberian" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">
                                                      <h5>Pengalihan Pengaduan Untuk Tindak Lanjut</h5>
                                                      <div class="mb-3 row">
                                                          <label for="bagian" class="col-sm-2 col-form-label">Berikan
                                                              Kepada Bagian
                                                          </label>
                                                          <div class="col-sm-6">
                                                              <input type="text" class="form-control" id="bagian">
                                                          </div>
                                                      </div>

                                                      <button class="btn btn-primary" id="submitbagian"></button>
                                                  </div>
                                              </div>
                                          </div>

                                          <div class="card mt-3" id="penerimaanpengaduan" style="display: none">
                                              <div class="card-body">
                                                  <div class="mt-3">

                                                      <h5>Penerimaan Pengaduan</h5>

                                                      <div class="mb-3 row">
                                                          <label for="penerimaan"
                                                              class="col-sm-2 col-form-label">Penerimaan
                                                              Pengaduan
                                                          </label>
                                                          <div class="col-sm-8">
                                                              <select class="form-select" id="penerimaan">
                                                                  <option value="" selected>Pilih</option>
                                                                  <option value="Diterima">Diterima</option>
                                                                  <option value="Ditolak">Ditolak</option>
                                                                  <option value="Ditangguhkan">Ditangguhkan</option>
                                                              </select>
                                                          </div>
                                                      </div>
                                                      <div class="mb-3 row">
                                                          <label for="ket_kasub"
                                                              class="col-sm-2 col-form-label">Keterangan</label>
                                                          <div class="col-sm-8">
                                                              <textarea name="ket_kasub" id="ket_kasub" class="form-control" cols="50"></textarea>
                                                          </div>
                                                      </div>
                                                      <button class="btn btn-primary" id="submitpenerimaan"></button>
                                                  </div>
                                              </div>
                                          </div>
                                      </form>
                                  </div>
                              </div>
                              {{-- tab tindaklanjut --}}
                              <div class="tab-pane fade " id="tindaklanjut" role="tabpanel">
                                  <div class="card mt-3">
                                      <div class="card-body">
                                          <div class="mt-3">
                                              <p>Tindak Lanjut Dilakukan Oleh Bagian Terkait Setelah Diberikan Penugasan
                                                  Oleh KA
                                                  Sub Bag</p>
                                              <p><i><b>Note : Uraian DIlihat Dari Detail Pengaduan</b></i></p>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="card mt-3" id="menunggubagian" style="display: none">
                                      <div class="card-body">
                                          <div class="mt-3">
                                              <h5><b>Menunggu Pengalihan Tugas Dari KA Sub Bag</b></h5>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="card mt-3" id="diterimabagian" style="display: none">
                                      <div class="card-body">
                                          <div class="mt-3">
                                              <h5><b>Pengaduan Diterima Oleh KA Sub Bag Dan Telah Di Alih Tugaskan</b></h5>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="card mt-3" id="ditanganibagian" style="display: none">
                                      <div class="card-body">
                                          <div class="mt-3">
                                              <h5><b>Pengaduan Telah Ditangani Oleh Bagian Terkait</b></h5>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="card mt-3" id="selesaibagian" style="display: none">
                                      <div class="card-body">
                                          <div class="mt-3">
                                              <h5><b>Pengaduan Dinyatakan Selesai</b></h5>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="card mt-3" id="hasilbagian">
                                      <div class="card-body">
                                          <div class="mt-3">

                                              <div class="mb-3 row">
                                                  <label for="waktu_bagian_hasil" class="col-sm-2 col-form-label">Waktu
                                                      Tindak
                                                      Lanjut
                                                  </label>
                                                  <div class="col-sm-8">
                                                      <input type="text" id="waktu_bagian_hasil" class="form-control"
                                                          readonly>
                                                  </div>

                                              </div>
                                              <div class="mb-3 row">
                                                  <label for="bagianmenerima_hasil" class="col-sm-2 col-form-label">Bagian
                                                      Terkait</label>
                                                  <div class="col-sm-8">
                                                      <input type="text" id="bagianmenerima_hasil"
                                                          class="form-control" readonly>
                                                  </div>
                                              </div>
                                              <div class="mb-3 row">
                                                  <label for="tindaklanjutbagian_hasil"
                                                      class="col-sm-2 col-form-label">Tindak
                                                      Lanjut
                                                  </label>
                                                  <div class="col-sm-8">
                                                      <input type="text" id="tindaklanjutbagian_hasil"
                                                          class="form-control" readonly>
                                                  </div>

                                              </div>
                                              <div class="mb-3 row">
                                                  <label for="ket_bagian_hasil"
                                                      class="col-sm-2 col-form-label">Keterangan</label>
                                                  <div class="col-sm-8">
                                                      <textarea readonly name="ket_bagian_hasil" id="ket_bagian_hasil" class="form-control" cols="50"></textarea>
                                                  </div>
                                              </div>
                                              <h5>Bukti File</h5>
                                              <table class="table" id="tablebuktibagian"></table>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="card mt-3" id="inputbagian">
                                      <div class="card-body">
                                          <div class="mt-3">
                                              <form id="formbag">
                                                  <div class="mb-3 row">
                                                      <label for="ket_bagian" class="col-sm-2 col-form-label">Bagian
                                                          Terkait</label>
                                                      <div class="col-sm-8">
                                                          <input type="text" id="bagianmenerima" class="form-control"
                                                              readonly>
                                                      </div>
                                                  </div>
                                                  <div class="mb-3 row">
                                                      <label for="tindaklanjutbagian"
                                                          class="col-sm-2 col-form-label">Tindak
                                                          Lanjut
                                                      </label>
                                                      <div class="col-sm-8">
                                                          <select class="form-select" id="tindaklanjutbagian">
                                                              <option value="" selected>Pilih</option>
                                                              <option value="Ditangani">Ditangani</option>
                                                              <option value="Tidak Tertangani">Tidak Tertangani
                                                              </option>

                                                          </select>
                                                      </div>
                                                  </div>

                                                  <div class="mb-3 row">
                                                      <label for="ket_bagian"
                                                          class="col-sm-2 col-form-label">Keterangan</label>
                                                      <div class="col-sm-8">
                                                          <textarea name="ket_bagian" id="ket_bagian" class="form-control" cols="50"></textarea>
                                                      </div>
                                                  </div>
                                                  <h4>Upload Bukti Tindak Lanjut</h4>
                                                  <div class="droppable-area" id="droppableArea">
                                                      Drag & Drop file di sini atau klik untuk memilih file.
                                                      <input type="file" id="fileInput" name="bukti[]" multiple
                                                          style="display: none;">
                                                  </div>

                                                  <div id="fileTable" style="display: none;">
                                                      <table class="table table-bordered">
                                                          <tbody id="fileList">
                                                          </tbody>
                                                      </table>
                                                  </div>
                                                  <button class="btn btn-primary"
                                                      id="submitbagiantindaklanjut">Kirim</button>

                                              </form>
                                          </div>

                                      </div>
                                  </div>
                              </div>
                              {{-- tab selesai --}}
                              <div class="tab-pane fade " id="penyelesaian" role="tabpanel">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="mt-3">
                                              <p>Penyelesaian Pengaduan</p>
                                              <p><i><b>Note : Apabila Pengaduan Sudah Berstatus Selesai Maka Pengaduan
                                                          Sudah
                                                          Tidak Dapat DI Respon Kembali</b></i></p>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="card mt-3" id="selesai_hasil" style="display: none">
                                      <div class="card-body">
                                          <div class="mt-3">
                                              <h4>Pengaduan Sudah Selesai</h4>
                                              <div class="mb-3 row">
                                                  <label for="waktuselesai_hasil" class="col-sm-2 col-form-label">Waktu
                                                      Penyelesaian</label>
                                                  <div class="col-sm-8">
                                                      <input type="text" id="waktuselesai_hasil" class="form-control"
                                                          readonly>
                                                  </div>

                                              </div>
                                              <div class="mb-3 row">
                                                  <label for="statusselesai_hasil" class="col-sm-2 col-form-label">Status
                                                      Pengaduan</label>
                                                  <div class="col-sm-8">
                                                      <input type="text" id="statusselesai_hasil" value="Selesai"
                                                          class="form-control" readonly>
                                                  </div>

                                              </div>
                                              <div class="mb-3 row">
                                                  <label for="ket_selesai_hasil"
                                                      class="col-sm-2 col-form-label">Keterangan</label>
                                                  <div class="col-sm-8">
                                                      <textarea readonly name="ket_selesai_hasil" id="ket_selesai_hasil" class="form-control" cols="50"></textarea>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>


                                  </div>
                                  <div class="card mt-3" id="inputselesai">
                                      <div class="card-body">
                                          <div class="mt-3">
                                              <form id="formselesai">
                                                  <div class="mb-3 row">
                                                      <label for="statusselsesai" class="col-sm-2 col-form-label">Status
                                                          Pengaduan</label>
                                                      <div class="col-sm-8">
                                                          <input type="text" id="statusselesai" value="Selesai"
                                                              class="form-control" readonly>
                                                      </div>

                                                  </div>
                                                  <div class="mb-3 row">
                                                      <label for="ket_selesai"
                                                          class="col-sm-2 col-form-label">Keterangan</label>
                                                      <div class="col-sm-8">
                                                          <textarea name="ket_selesai" id="ket_selesai" class="form-control" cols="50"></textarea>
                                                      </div>
                                                  </div>
                                          </div>
                                          </form>
                                          <div>
                                              <button class="btn btn-primary" id="submitselesai">
                                                  Selesaikan Pengaduan
                                              </button>
                                          </div>
                                      </div>


                                  </div>
                              </div>
                              {{-- tab status --}}
                              <div class="tab-pane fade " id="status" role="tabpanel">
                                  <div class="card mt-3">
                                      <div class="card-body">
                                          <div class="mt-3">
                                              <table class="table">
                                                  <tr>
                                                      <th>Waktu</th>
                                                      <th>Status</th>
                                                      <th>Keterangan</th>
                                                  </tr>

                                                  <tr id="trpengaduan" style="display: none">
                                                      <td><span id="waktu_pengaduan"></span></td>
                                                      <td><span id="status_pengaduan"></span></td>
                                                      <td><span id="ket_pengaduan"></span></td>
                                                  </tr>
                                                  <tr id="trverifikasi" style="display: none">
                                                      <td><span id="waktu_verifikasi_status"></span></td>
                                                      <td><span id="verifikasi_status"></span></td>
                                                      <td id="ket_verifikasi_status"></td>

                                                  </tr>
                                                  <tr id="trka" style="display: none">
                                                      <td><span id="waktu_ka_status"></span></td>
                                                      <td><span id="ka_status"></span></td>
                                                      <td id="ket_ka_status"></td>
                                                  </tr>
                                                  <tr id="trbagian" style="display: none">
                                                      <td><span id="waktu_bagian_status"></span></td>
                                                      <td><span id="bagian_status"></span></td>
                                                      <td id="ket_bagian_status"></td>
                                                  </tr>
                                                  <tr id="trselesai" style="display: none">
                                                      <td><span id="waktu_selesai_status"></span></td>
                                                      <td><span id="selesai_status"></span></td>
                                                      <td><span id="ket_selesai_status"></span></td>
                                                  </tr>

                                              </table>
                                          </div>
                                      </div>
                                  </div>
                              </div>


                          </div>
                      </div>

                  </div>
                  {{-- <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  </div> --}}

              </div>
          </div>
      </div>

      <div class="modal fade" id="modalpencarian" tabindex="-1" aria-labelledby="fileModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable modal-xl">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="fileModalLabel">Pencarian</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                      <div class="card">
                          <div class="card-body">
                              <form action="" id="formpencarian">
                                  <div class="mb-3 row">
                                      <label class="col-sm-2 col-form-label">Tanggal Pengaduan
                                      </label>
                                      <div class="col-sm-4">
                                          <input type="text" autocomplete="off" data-toggle="datepicker"
                                              class="form-control" id="tanggalawal" name="tanggalawal"
                                              placeholder="DD-MM-YYYY">
                                      </div>
                                      <div class="col-sm-4">
                                          <input type="text" autocomplete="off" data-toggle="datepicker"
                                              class="form-control" id="tanggalakhir" name="tanggalakhir"
                                              placeholder="DD-MM-YYYY">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                                      <div class="col-sm-8">
                                          <input type="text" class="form-control" name="nama" id="nama">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="nohp" class="col-sm-2 col-form-label">No Telp</label>
                                      <div class="col-sm-8">
                                          <input type="text" class="form-control" name="nohp" id="nohp">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="email" class="col-sm-2 col-form-label">Email</label>
                                      <div class="col-sm-8">
                                          <input type="text" class="form-control" name="email" id="email">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="token" class="col-sm-2 col-form-label">Tiket</label>
                                      <div class="col-sm-8">
                                          <input type="text" class="form-control" name="token" id="token">
                                      </div>
                                  </div>
                                  <div class="mb-3 row">
                                      <label for="status" class="col-sm-2 col-form-label">Status Pengaduan</label>
                                      <div class="col-sm-8">
                                          <select class="form-select" id="statuspencarian">
                                              <option selected value="">Pilih</option>
                                              <option value="Pengaduan Baru">Pengaduan Baru</option>
                                              <option value="Diverifikasi Verifikator">Diverifikasi</option>
                                              <option value="Ditolak Verifikator">Ditolak Verifikator</option>
                                              <option value="Diterima KA Sub Bag">Diterima</option>
                                              <option value="Ditolak KA Sub Bag">Ditolak KA Sub Bag</option>
                                              <option value="Diterima Bagian Terkait">Diterima Bagian Terkait</option>
                                              <option value="Ditangani">Ditangani</option>
                                              <option value="Tidak Tertangani">Tidak Tertangani</option>
                                              <option value="Selesai">Selesai</option>
                                          </select>
                                      </div>
                                  </div>



                          </div>
                      </div>
                  </div>

                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" id="tampilkan">Tampilkan</button>

                  </div>
                  </form>
              </div>
          </div>
      </div>

      <div class="modal fade" id="fileModal" tabindex="-1" aria-labelledby="fileModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable modal-xl">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="fileModalLabel">Preview File</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                      <div id="fileContent"></div>
                  </div>
              </div>
          </div>
      </div>
      <script src="{{ asset('date/js/datepicker.js') }}"></script>
      <script>
          function lakukanPencarian() {
              var tanggalawal = $('#tanggalawal').val();
              var tanggalakhir = $('#tanggalakhir').val();
              var nama = $('#nama').val();
              var nohp = $('#nohp').val();
              var email = $('#email').val();
              var token = $('#token').val();
              var status = $('#statuspencarian').val();

              // Memeriksa apakah setidaknya satu input diisi
              if (tanggalawal || tanggalakhir || nama || nohp || email || token || status) {
                  var dataParams = {
                      'tanggalawal': tanggalawal,
                      'tanggalakhir': tanggalakhir,
                      'nama': nama,
                      'nohp': nohp,
                      'email': email,
                      'token': token,
                      'status': status,
                  };

                  $('#table').DataTable().ajax.url('/pencarian_wbs?' + $.param(
                      dataParams)).load();
                  console.log('/pencarian_wbs?' + $.param(dataParams));
              } else {
                  // Jika tidak ada input yang diisi, reload tabel tanpa filter
                  $('#table').DataTable().ajax.url('/list_wbs').load();

              }
          }

          function formatDate(date) {
              var day = String(date.getDate()).padStart(2, '0');
              var month = String(date.getMonth() + 1).padStart(2, '0'); // January is 0!
              var year = date.getFullYear();
              var hours = String(date.getHours()).padStart(2, '0');
              var minutes = String(date.getMinutes()).padStart(2, '0');

              return day + '-' + month + '-' + year + ' (' + hours + ':' + minutes + ')';
          }

          function tanggalsaja(date) {
              var day = String(date.getDate()).padStart(2, '0');
              var month = String(date.getMonth() + 1).padStart(2, '0');
              var year = date.getFullYear();

              return day + '-' + month + '-' + year;
          }

          function jumlah() {
              $.ajax({
                  url: '/jumlah_wbs',
                  type: 'GET',
                  dataType: 'json',
                  success: function(res) {

                      $('#jumlah').text(res.jumlah);



                  },
                  error: function(xhr, status, error) {
                      console.error(xhr.responseText); // Menampilkan pesan kesalahan dalam konsol
                  }
              });
          }

          function status_track(id) {
              $.ajax({
                  url: '/status_track/' + id,
                  type: 'GET',
                  dataType: 'json',
                  success: function(res) {
                      $("#trpengaduan").attr("style", "display:null");
                      var time_pengaduan = new Date(res.created_at);
                      var tanggal_pengaduan = formatDate(time_pengaduan);
                      $('#waktu_pengaduan').text(tanggal_pengaduan);
                      $('#status_pengaduan').text("Pengaduan Baru");
                      $('#ket_pengaduan').text("Sedang Di Proses");

                      if (res.status !== null) {
                          if (res.status !== "Pengaduan Baru") {
                              $('#ket_pengaduan').text("Sedang Di Tanggapi");
                              $("#trverifikasi").attr("style", "display:null");
                              var time_verifikasi = new Date(res.waktu_verifikasi);
                              var tanggal_verifikasi = formatDate(time_verifikasi);
                              $('#waktu_verifikasi_status').text(tanggal_verifikasi);
                              $('#verifikasi_status').text(res.status);
                              $('#ket_verifikasi_status').html('<span>' + res.respon_verifikasi +
                                  '</span>' +
                                  '<br><span><i>Dilakukan Oleh Verifikator</i></span>');
                          }

                      }
                      if (res.status_ka !== null) {
                          $("#trka").attr("style", "display:null");
                          var time_ka = new Date(res.waktu_ka);
                          var tanggal_ka = formatDate(time_ka);
                          $('#waktu_ka_status').text(tanggal_ka);
                          $('#ka_status').text(res.status_ka);
                          $('#ket_ka_status').html('<span>' + res.respon_ka + '</span>' +
                              '<br><span><i>Dilakukan Oleh KA Sub Bagian</i></span>');
                      }
                      if (res.status_bagian !== null) {
                          $("#trbagian").attr("style", "display:null");
                          var time_bagian = new Date(res.waktu_pemberian_bagian);
                          var tanggal_bagian = formatDate(time_bagian);
                          $('#waktu_bagian_status').text(tanggal_bagian);
                          $('#bagian_status').text(res.status_pemberian_bagian);
                          $('#ket_bagian_status').html(
                              '<span>KA Sub Bag Sudah Melakukan Penugasan Kepada Bagian ' + res
                              .bagian + '</span>');
                      }
                      if (res.status_bagian !== null) {
                          $("#trbagian").attr("style", "display:null");
                          var time_bagian = new Date(res.waktu_bagian);
                          var tanggal_bagian = formatDate(time_bagian);
                          $('#waktu_bagian_status').text(tanggal_bagian);
                          $('#bagian_status').text(res.status_bagian);
                          $('#ket_bagian_status').html('<span>' + res.ket_bagian + '</span>' +
                              '<br><span><i>Dilakukan Oleh Bagian ' + res
                              .bagian + '</i></span>');
                      }
                      if (res.status_selesai !== null) {
                          $("#trselesai").attr("style", "display:null");
                          var time_selesai = new Date(res.waktu_selesai);
                          var tanggal_selesai = formatDate(time_selesai);
                          $('#waktu_selesai_status').text(tanggal_selesai);
                          $('#selesai_status').text(res.status_selesai);
                          $('#ket_selesai_status').text(res.ket_selesai);
                      }






                  },
                  error: function(xhr, status, error) {
                      console.error(xhr.responseText); // Menampilkan pesan kesalahan dalam konsol
                  }
              });
          }

          function reloadTable(url) {
              lakukanPencarian();
              jumlah();

          }

          function Data(url) {
              $('#table').DataTable({
                  processing: true,
                  serverSide: true,
                  responsive: true,
                  ajax: url,

                  "columnDefs": [{
                      "searchable": false,
                      "orderable": false,
                      "targets": 0 // Kolom nomor
                  }],
                  "order": [
                      [0, 'asc']
                  ],
                  columns: [{
                          data: null,
                          orderable: false,
                          searchable: false,
                          render: function(data, type, row, meta) {
                              return meta.row + meta.settings._iDisplayStart + 1;
                          }
                      },
                      {
                          data: 'tanggal'
                      },
                      {
                          data: 'nama'
                      },

                      {
                          data: 'token'
                      },
                      {
                          data: 'status'
                      },
                      {
                          data: 'tombol',
                          name: 'tombol',
                          orderable: false,
                          searchable: false,
                      },
                  ]
              });
          }

          function verifikasi(id) {
              $.ajax({
                  url: '/status_verifikasi/' + id,
                  type: 'GET',
                  dataType: 'json',
                  success: function(res) {

                      if (res.status === 'Belum') {
                          $('#submitverifikator').text('Submit');
                          $("#isiverifikator").attr("style", "display:none");
                          $("#formverifikator").attr("style", "display:block");
                      } else if (res.status === 'Diverifikasi') {

                          $("#verifikasi_ditolak").attr("style", "display:none");
                          var time = new Date(res.waktu);
                          var tanggal = formatDate(time);
                          $('#waktu_verifikasi').val(tanggal);
                          $('#verifikasi_hasil').val(res.verifikator);
                          $('#ket_verifikator_hasil').val(res.keterangan);
                          $('#submitverifikator').text('Update');
                          $('#verifikasi').val(res.verifikator);
                          $('#ket_verifikator').val(res.keterangan);
                          $("#isiverifikator").attr("style", "display:");
                          if (res.selesai === "selesai") {
                              $("#verifikasi_selesai").attr("style", "display:block");
                              $("#verifikasi_diterima").attr("style", "display:none");
                              $("#formverifikator").attr("style", "display:none");
                          } else {
                              $("#verifikasi_selesai").attr("style", "display:none");
                              $("#verifikasi_diterima").attr("style", "display:block");
                              $("#formverifikator").attr("style", "display:block");
                          }
                      } else if (res.status === 'Ditolak') {
                          var time = new Date(res.waktu);
                          var tanggal = formatDate(time);
                          $('#waktu_verifikasi').val(tanggal);
                          $('#verifikasi_hasil').val(res.verifikator);
                          $('#ket_verifikator_hasil').val(res.keterangan);
                          $("#verifikasi_diterima").attr("style", "display:none");
                          $("#verifikasi_ditolak").attr("style", "display:block");
                          $("#isiverifikator").attr("style", "display:block");
                          $("#formverifikator").attr("style", "display:none");
                          if (res.selesai === "selesai") {
                              $("#verifikasi_selesai").attr("style", "display:none");
                          }
                      }



                  },
                  error: function(xhr, status, error) {
                      console.error(xhr.responseText); // Menampilkan pesan kesalahan dalam konsol
                  }
              });
          }

          function disposisi(id) {
              $.ajax({
                  url: '/status_disposisi/' + id,
                  type: 'GET',
                  dataType: 'json',
                  success: function(res) {
                      $('#penerimaan').prop('disabled', false);
                      $("#ditolakverifikasi").attr("style", "display:none");
                      $("#belumverifikasi").attr("style", "display:none");
                      $("#sudahverifikasi").attr("style", "display:none");
                      $("#diterimakasubbag").attr("style", "display:none");
                      $("#ditolakkasubbag").attr("style", "display:none");
                      $("#pemberiansukses").attr("style", "display:none");
                      $("#pemberianditangguhkan").attr("style", "display:none");
                      $("#selesaidisposisi").attr("style", "display:none");
                      $("#pemberianhasil").attr("style", "display:none");
                      $("#hasildisposisi").attr("style", "display:none");
                      $("#pemberian").attr("style", "display:none");
                      $("#penerimaanpengaduan").attr("style", "display:none");
                      if (res.status === 'belum diverifikasi') {
                          $("#belumverifikasi").attr("style", "display:block");
                      }
                      if (res.status === 'belum') {

                          $("#penerimaanpengaduan").attr("style", "display:block");
                          $('#submitpenerimaan').text('Submit');
                          $("#sudahverifikasi").attr("style", "display:block");

                      }
                      if (res.status === 'ditolak_kasubag') {
                          if (res.data.status === "Ditolak Verifikator") {
                              $("#ditolakverifikasi").attr("style", "display:block");
                          }
                          if (res.data.status === "Diverifikasi Verifikator") {
                              $("#sudahverifikasi").attr("style", "display:block");
                          }
                          if (res.data.status_ka === "Diterika KA Sub Bag") {
                              $("#diterimakasubbag").attr("style", "display:block");
                          }
                          if (res.data.status_ka === "Ditolak KA Sub Bag") {
                              $("#ditolakkasubbag").attr("style", "display:block");
                          }
                          if (res.data.status_ka === "Di Tindak Lanjuti") {
                              $("#pemberianhasil").attr("style", "display:block");
                              $("#pemberiansukses").attr("style", "display:block");
                              var time = new Date(res.data.waktu_pemberian_bagian);
                              var tanggal = formatDate(time);
                              $('#waktubagianhasil').val(tanggal);
                              $('#bagianhasil').val(res.data.bagian);
                          }
                          if (res.data.status_ka === "Di Tangguhkan") {
                              $("#pemberianditangguhkan").attr("style", "display:block");
                          }

                          $("#hasildisposisi").attr("style", "display:block");
                          $("#selesaidisposisi").attr("style", "display:block");
                          var time = new Date(res.data.waktu_ka);
                          var tanggal = formatDate(time);
                          $('#waktupenerimaanhasil').val(tanggal);
                          $('#penerimaanhasil').val("Diterima");
                          $('#ket_kasub_hasil').val(res.data.respon_ka);


                      }
                      if (res.status === 'ditolak_verifikator') {


                          $("#ditolakverifikasi").attr("style", "display:block");

                          $("#selesaidisposisi").attr("style", "display:block");

                      }
                      if (res.status === 'ditangguhkan') {
                          $('#penerimaan').val('Ditangguhkan');
                          $('#ket_kasub').val(res.data.respon_ka);
                          $("#pemberianditangguhkan").attr("style", "display:block");
                          $("#penerimaanpengaduan").attr("style", "display:block");
                          $('#submitpenerimaan').text('Upadate');
                          $("#sudahverifikasi").attr("style", "display:block");
                      }
                      if (res.status === 'diterima') {
                          $("#pemberian").attr("style", "display:block");
                          $("#penerimaanpengaduan").attr("style", "display:block");
                          $('#submitpenerimaan').text('Upadate');
                          $('#penerimaan').val('Diterima');
                          $('#ket_kasub').val(res.data.respon_ka);
                          if (res.data.bagian === null) {

                              $('#submitbagian').text('Berikan Penugasan');
                          } else {
                              $('#bagian').val(res.data.bagian);
                              $('#submitbagian').text('Update Penugasan');
                          }

                          $("#sudahverifikasi").attr("style", "display:block");
                          $("#diterimakasubbag").attr("style", "display:block");
                      }
                      if (res.status === 'ditindaklanjut') {
                          if (res.data.status === "Ditolak Verifikator") {
                              $("#ditolakverifikasi").attr("style", "display:block");
                          }
                          if (res.data.status === "Diverifikasi Verifikator") {
                              $("#sudahverifikasi").attr("style", "display:block");
                          }
                          if (res.data.status_ka === "Diterika KA Sub Bag") {
                              $("#diterimakasubbag").attr("style", "display:block");
                          }
                          if (res.data.status_ka === "Ditolak KA Sub Bag") {
                              $("#ditolakkasubbag").attr("style", "display:block");
                          }
                          if (res.data.status_ka === "Di Tindak Lanjuti") {
                              $("#pemberianhasil").attr("style", "display:block");
                              $("#pemberiansukses").attr("style", "display:block");
                              var time = new Date(res.data.waktu_pemberian_bagian);
                              var tanggal = formatDate(time);
                              $('#waktubagianhasil').val(tanggal);
                              $('#bagianhasil').val(res.data.bagian);
                          }
                          if (res.data.status_ka === "Di Tangguhkan") {
                              $("#pemberianditangguhkan").attr("style", "display:block");
                          }
                          if (res.data.status_selesai === "Selesai") {
                              $("#selesaidisposisi").attr("style", "display:block");
                              var time = new Date(res.data.waktu_ka);
                              var tanggal = formatDate(time);
                              $('#waktupenerimaanhasil').val(tanggal);
                              $('#penerimaanhasil').val(res.data.status_ka);
                              $('#ket_kasub_hasil').val(res.data.respon_ka);
                              $("#hasildisposisi").attr("style", "display:block");
                              $('#waktupenerimaanhasil').val(tanggal);
                              $('#penerimaanhasil').val("Diterima");
                              $('#ket_kasub_hasil').val(res.data.respon_ka);
                          }
                          if (res.data.status_selesai !== "Selesai") {
                              $("#pemberian").attr("style", "display:block");
                              $("#penerimaanpengaduan").attr("style", "display:block");
                              $('#submitpenerimaan').text('Update');
                              $('#bagian').val(res.data.bagian);
                              $('#penerimaan').val('Diterima');
                              $('#ket_kasub').val(res.data.respon_ka);
                              if (res.data.bagian === null) {
                                  $('#submitbagian').text('Berikan Penugasan');
                              } else {
                                  $('#submitbagian').text('Update Penugasan');
                              }
                              $('#penerimaan').prop('disabled', true);


                          }






                      }
                      if (res.status === "selesai") {
                          if (res.data.status === "Ditolak Verifikator") {
                              $("#ditolakverifikasi").attr("style", "display:block");
                          }
                          if (res.data.status === "Diverifikasi Verifikator") {
                              $("#sudahverifikasi").attr("style", "display:block");
                          }
                          if (res.data.status_ka === "Diterika KA Sub Bag") {
                              $("#diterimakasubbag").attr("style", "display:block");
                          }
                          if (res.data.status_ka === "Ditolak KA Sub Bag") {
                              $("#ditolakkasubbag").attr("style", "display:block");
                          }
                          if (res.data.status_ka === "Di Tindak Lanjuti") {
                              $("#pemberianhasil").attr("style", "display:block");
                              $("#pemberiansukses").attr("style", "display:block");
                              var time = new Date(res.data.waktu_pemberian_bagian);
                              var tanggal = formatDate(time);
                              $('#waktubagianhasil').val(tanggal);
                              $('#bagianhasil').val(res.data.bagian);


                          }
                          if (res.data.status_ka === "Di Tangguhkan") {
                              $("#pemberianditangguhkan").attr("style", "display:block");
                          }
                          $("#selesaidisposisi").attr("style", "display:block");
                          $("#hasildisposisi").attr("style", "display:block");
                          var time = new Date(res.data.waktu_ka);
                          var tanggal = formatDate(time);
                          $('#waktupenerimaanhasil').val(tanggal);
                          $('#penerimaanhasil').val("Diterima");
                          $('#ket_kasub_hasil').val(res.data.respon_ka);



                      }



                  },
                  error: function(xhr, status, error) {
                      console.error(xhr.responseText); // Menampilkan pesan kesalahan dalam konsol
                  }
              });
          }

          function tindaklanjutstatus(id) {
              $.ajax({
                  url: '/status_tindaklanjut/' + id,
                  type: 'GET',
                  dataType: 'json',
                  success: function(res) {

                      if (res.status === 'belum ditangani') {
                          $("#menunggubagian").attr("style", "display:none");
                          $("#ditanganibagian").attr("style", "display:none");
                          $("#diterimabagian").attr("style", "display:block");
                          $("#selesaibagian").attr("style", "display:none");

                          $("#hasilbagian").attr("style", "display:none");
                          $("#inputbagian").attr("style", "display:null");
                          $("#bagianmenerima").val(res.data.bagian);
                      }
                      if (res.status === 'ditangani') {
                          if (res.data.status_bagian === "Ditangani" || res.data.status_bagian ===
                              "Tidak Tertangani") {
                              $("#menunggubagian").attr("style", "display:none");
                              $("#ditanganibagian").attr("style", "display:block");
                              $("#diterimabagian").attr("style", "display:block");
                              $("#selesaibagian").attr("style", "display:none");
                              $("#hasilbagian").attr("style", "display:null");
                              $("#inputbagian").attr("style", "display:none");
                              $("#bagianmenerima_hasil").val(res.data.bagian);
                              $("#tindaklanjutbagian_hasil").val(res.data.status_bagian);
                              $("#ket_bagian_hasil").val(res.data.ket_bagian);
                              var waktu = new Date(res.data.waktu_bagian);
                              var tanggal = formatDate(waktu);
                              $("#waktu_bagian_hasil").val(tanggal);
                              var bukti = res.bukti;
                              var tableContent = '';

                              for (var i = 0; i < bukti.length; i++) {
                                  var row = '<tr>' +
                                      '<td>' + (i + 1) + '</td>' +
                                      '<td>' + bukti[i].nama_file + '</td>' +
                                      '<td><button class="btn btn-primary lihat_file" data-id="' +
                                      bukti[i].path +
                                      '"><i class="bi-eye"></i></button></td>' +
                                      '<td><button class="btn btn-success unduh_file" data-id="' +
                                      bukti[i].path +
                                      '"><i class="bi-download"></i></button></td>' +
                                      '</tr>';
                                  tableContent += row;
                              }
                              $('#tablebuktibagian').html(tableContent);
                          } else {
                              $("#selesaibagian").attr("style", "display:block");
                          }
                      }
                      if (res.status === 'belum diterima') {
                          $("#menunggubagian").attr("style", "display:block");
                          $("#ditanganibagian").attr("style", "display:none");
                          $("#diterimabagian").attr("style", "display:none");
                          $("#selesaibagian").attr("style", "display:none");

                          $("#hasilbagian").attr("style", "display:none");
                          $("#inputbagian").attr("style", "display:none");
                      }
                      if (res.status === 'selesai') {
                          if (res.data.status_bagian === "Ditangani" || res.data.status_bagian ===
                              "Tidak Tertangani") {
                              $("#menunggubagian").attr("style", "display:none");
                              $("#ditanganibagian").attr("style", "display:block");
                              $("#diterimabagian").attr("style", "display:block");
                              $("#selesaibagian").attr("style", "display:block");
                              $("#hasilbagian").attr("style", "display:null");
                              $("#inputbagian").attr("style", "display:none");
                              $("#bagianmenerima_hasil").val(res.data.bagian);
                              $("#tindaklanjutbagian_hasil").val(res.data.status_bagian);
                              $("#ket_bagian_hasil").val(res.data.ket_bagian);
                              var waktu = new Date(res.data.waktu_bagian);
                              var tanggal = formatDate(waktu);
                              $("#waktu_bagian_hasil").val(tanggal);
                              var bukti = res.bukti;
                              var tableContent = '';

                              for (var i = 0; i < bukti.length; i++) {
                                  var row = '<tr>' +
                                      '<td>' + (i + 1) + '</td>' +
                                      '<td>' + bukti[i].nama_file + '</td>' +
                                      '<td><button class="btn btn-primary lihat_file" data-id="' +
                                      bukti[i].path +
                                      '"><i class="bi-eye"></i></button></td>' +
                                      '<td><button class="btn btn-success unduh_file" data-id="' +
                                      bukti[i].path +
                                      '"><i class="bi-download"></i></button></td>' +
                                      '</tr>';
                                  tableContent += row;
                              }
                              $('#tablebuktibagian').html(tableContent);
                          } else {
                              $("#selesaibagian").attr("style", "display:block");
                          }
                      }




                  },
                  error: function(xhr, status, error) {
                      console.error(xhr.responseText); // Menampilkan pesan kesalahan dalam konsol
                  }
              });
          }

          function selesai(id) {
              $.ajax({
                  url: '/status_selesai/' + id,
                  type: 'GET',
                  dataType: 'json',
                  success: function(res) {

                      if (res.status === 'selesai') {
                          $("#inputselesai").attr("style", "display:none");
                          $("#selesai_hasil").attr("style", "display:block");
                          var waktu = new Date(res.data.waktu_selesai);
                          var tanggal = formatDate(waktu);
                          $("#waktuselesai_hasil").val(tanggal);
                          $("#statusselesai_hasil").val(res.data.status_selesai);
                          $("#ket_selesai_hasil").val(res.data.ket_selesai);


                      } else {
                          $("#inputselesai").attr("style", "display:null");
                          $("#selesai_hasil").attr("style", "display:none");


                      }



                  },
                  error: function(xhr, status, error) {
                      console.error(xhr.responseText); // Menampilkan pesan kesalahan dalam konsol
                  }
              });
          }

          //fungsi langsung
          $(document).ready(function() {
              jumlah();

              Data('/list_wbs');
              $('#tutup').click(function() {
                  $('#formverif')[0].reset();
                  $('#formdispo')[0].reset();
                  $('#formselesai')[0].reset();
                  $('#formbag')[0].reset();
              });
              $('[data-toggle="datepicker"]').datepicker({
                  autoHide: true,
                  zIndex: 2048,
                  format: 'dd-mm-yyyy',
              });

              $('#tampilkan').click(function() {
                  lakukanPencarian();
                  $('#modalpencarian').modal('hide');
              });
              $('#pencarian').click(function() {
                  $('#modalpencarian').modal('show');
              });
              $('#table').on('click', '.proses', function() {

                  var id = $(this).data('id');
                  $('#idverifikator').val(id);
                  verifikasi(id);
                  $('#modalproses').modal('show');

              });
              $('#disposisibtn').click(function() {
                  var id = $('#idverifikator').val();
                  disposisi(id);
              });
              $('#selesaibtn').click(function() {
                  var id = $('#idverifikator').val();
                  selesai(id);
              });
              $('#statusbtn').click(function() {
                  var id = $('#idverifikator').val();
                  status_track(id);
              });
              $('#verifikatorbtn').click(function() {
                  var id = $('#idverifikator').val();
                  verifikasi(id);
              });
              $('#tindaklanjutbtn').click(function() {
                  var id = $('#idverifikator').val();
                  tindaklanjutstatus(id);
              });
              $('#submitverifikator').click(function() {
                  var id = $('#idverifikator').val();
                  var status = $('#verifikasi').val();
                  var keterangan = $('#ket_verifikator').val();
                  if (!status) {
                      Swal.fire({
                          icon: 'error',
                          title: 'Oops...',
                          text: 'Pilih Verifikasi Terlebih Dahulu!',
                      });
                      return;
                  }
                  Swal.fire({
                      title: 'Konfirmasi',
                      text: 'Apakah Anda Yakin?',
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Ya!',
                      cancelButtonText: 'Batal'
                  }).then((result) => {
                      if (result.isConfirmed) {
                          // Jika dikonfirmasi, lakukan permintaan AJAX
                          $.ajax({
                              type: "GET",
                              url: "/submit_verifikator",
                              data: {
                                  id: id,
                                  status: status,
                                  keterangan: keterangan,
                              },
                              success: function(response) {
                                  lakukanPencarian();
                                  if (response.status == 'berhasil') {
                                      verifikasi(response.id);
                                      Swal.fire({
                                          icon: 'success',
                                          title: 'Berhasil',
                                          text: 'pengaduan' + ' ' + response
                                              .keterangan,
                                      });
                                  }

                              },
                              error: function(xhr, status, error) {
                                  console.error(xhr.responseText);
                              }
                          });
                      }
                  });

              });
              $('#submitpenerimaan').click(function() {
                  var id = $('#idverifikator').val();
                  var penerimaan = $('#penerimaan').val();
                  var keterangan = $('#ket_kasub').val();
                  if (!penerimaan) {
                      Swal.fire({
                          icon: 'error',
                          title: 'Oops...',
                          text: 'Pilih Penerimaan Pengaduan Terlebih Dahulu!',
                      });
                      return;
                  }
                  Swal.fire({
                      title: 'Konfirmasi',
                      text: 'Apakah Anda Yakin?',
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Ya!',
                      cancelButtonText: 'Batal'
                  }).then((result) => {
                      if (result.isConfirmed) {
                          // Jika dikonfirmasi, lakukan permintaan AJAX
                          $.ajax({
                              type: "GET",
                              url: "/submit_penerimaan",
                              data: {
                                  id: id,
                                  penerimaan: penerimaan,
                                  keterangan: keterangan,
                              },
                              success: function(response) {
                                  lakukanPencarian();
                                  if (response.status == 'berhasil') {
                                      disposisi(response.id);
                                      Swal.fire({
                                          icon: 'success',
                                          title: 'Berhasil',
                                          text: 'Pengaduan ' + ' ' + response
                                              .keterangan,
                                      });
                                  }

                              },
                              error: function(xhr, status, error) {
                                  console.error(xhr.responseText);
                              }
                          });
                      }
                  });

              });
              $('#submitbagian').click(function() {
                  var id = $('#idverifikator').val();
                  var bagian = $('#bagian').val();

                  if (!bagian) {
                      Swal.fire({
                          icon: 'error',
                          title: 'Oops...',
                          text: 'Pilih Bagian Terkait Terlebih Dahulu!',
                      });
                      return;
                  }
                  Swal.fire({
                      title: 'Konfirmasi',
                      text: 'Apakah Anda Yakin?',
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Ya!',
                      cancelButtonText: 'Batal'
                  }).then((result) => {
                      if (result.isConfirmed) {
                          // Jika dikonfirmasi, lakukan permintaan AJAX
                          $.ajax({
                              type: "GET",
                              url: "/submit_bagian",
                              data: {
                                  id: id,
                                  bagian: bagian,

                              },
                              success: function(response) {
                                  lakukanPencarian();
                                  if (response.status == 'berhasil') {
                                      disposisi(response.id);
                                      Swal.fire({
                                          icon: 'success',
                                          title: 'Berhasil',
                                          text: response
                                              .keterangan,
                                      });
                                  }

                              },
                              error: function(xhr, status, error) {
                                  console.error(xhr.responseText);
                              }
                          });
                      }
                  });

              });
              $('#submitbagiantindaklanjut').click(function(e) {
                  e.preventDefault(); // Mencegah form dari pengiriman (submit) normal

                  // Mengambil data dari form
                  var id = $('#idverifikator').val();
                  var bagianmenerima = $('#bagianmenerima').val();
                  var tindaklanjut = $('#tindaklanjutbagian').val();
                  var ket_bagian = $('#ket_bagian').val();
                  var files = $('#fileInput')[0].files;
                  var csrf_token = $('meta[name="csrf-token"]').attr('content');

                  // Menampilkan konfirmasi dengan SweetAlert 2
                  Swal.fire({
                      title: 'Konfirmasi',
                      text: 'Apakah Anda yakin ?',
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Ya, Kirim!',
                      cancelButtonText: 'Batal'
                  }).then((result) => {
                      if (result.isConfirmed) {
                          // Membuat objek FormData
                          var formData = new FormData();
                          formData.append('_token', csrf_token);
                          formData.append('bagianmenerima', bagianmenerima);
                          formData.append('id', id);
                          formData.append('tindaklanjut', tindaklanjut);
                          formData.append('ket_bagian', ket_bagian);
                          for (var i = 0; i < files.length; i++) {
                              formData.append('bukti[]', files[i]);
                          }

                          // Mengirim data dengan AJAX
                          $.ajax({
                              url: '/submittindaklanjutbagian',
                              method: 'POST',
                              data: formData,
                              contentType: false,
                              processData: false,
                              headers: {
                                  'X-CSRF-TOKEN': csrf_token // Menambahkan CSRF token ke header
                              },
                              success: function(response) {
                                  lakukanPencarian();
                                  // Handle response dari server

                                  tindaklanjutstatus(id);
                                  Swal.fire({
                                      icon: 'success',
                                      title: 'Berhasil',
                                      text: 'Berhasil Ditangani',
                                  });
                              },
                              error: function(xhr, status, error) {
                                  // Handle error
                                  console.error(xhr.responseText);
                                  tindaklanjut(id);
                                  Swal.fire({
                                      icon: 'error',
                                      title: 'Gagal',
                                      text: 'Terjadi kesalahan saat mengirim data.',
                                  });
                              }
                          });
                      }
                  });
              });
              $('#submitselesai').click(function() {
                  var id = $('#idverifikator').val();
                  var status = $('#statusselesai').val();
                  var keterangan = $('#ket_selesai').val();

                  Swal.fire({
                      title: 'Konfirmasi',
                      text: 'Apakah Anda Yakin?',
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Ya!',
                      cancelButtonText: 'Batal'
                  }).then((result) => {
                      if (result.isConfirmed) {
                          // Jika dikonfirmasi, lakukan permintaan AJAX
                          $.ajax({
                              type: "GET",
                              url: "/submit_selesai",
                              data: {
                                  id: id,
                                  status: status,
                                  keterangan: keterangan,
                              },
                              success: function(response) {
                                  lakukanPencarian();
                                  if (response.status == 'berhasil') {
                                      selesai(id);
                                      Swal.fire({
                                          icon: 'success',
                                          title: 'Berhasil',
                                          text: 'Pengaduan Berhasil Di Selesaikan',
                                      });
                                  }

                              },
                              error: function(xhr, status, error) {
                                  console.error(xhr.responseText);
                              }
                          });
                      }
                  });

              });


              $('#modalproses').on('show.bs.modal', function() {
                  // Set semua tab ke aria-selected="false"
                  $('#modalproses .nav-link').attr('aria-selected', 'false');

                  // Hapus class 'active' dari semua nav-link
                  $('#modalproses .nav-link').removeClass('active');

                  // Set aria-selected="true" untuk tombol dengan data-bs-target="#verifikator"
                  $('button[data-bs-target="#verifikator"]').attr('aria-selected', 'true');

                  // Tambahkan class 'active' pada tombol dengan data-bs-target="#verifikator"
                  $('button[data-bs-target="#verifikator"]').addClass('active');

                  // Hapus class 'show' dan 'active' dari semua tab-pane
                  $('#modalproses .tab-pane').removeClass('show active');

                  // Tambahkan class 'show' dan 'active' pada tab-pane dengan id 'verifikator'
                  $('#verifikator').addClass('show active');
              });

              $('#table').on('click', '.detail', function() {
                  var id = $(this).data('id');
                  $.ajax({
                      url: '/detail_pengaduan/' + id,
                      type: 'GET',
                      dataType: 'json',
                      success: function(data) {
                          $('#modaldetail').modal('show');
                          var createdAt = new Date(data.pengaduan.created_at);
                          var tanggal_pengaduan = formatDate(createdAt);
                          if (data.pengaduan.tgllahir === null) {
                              var tanggal_lahir = "-";

                          } else {
                              var tgllahir = new Date(data.pengaduan.tgllahir);
                              var tanggal_lahir = tanggalsaja(tgllahir);
                          }

                          var kejadian = new Date(data.pengaduan.tanggal);
                          var tanggal_kejadian = tanggalsaja(kejadian);
                          $('#namadetail').text(data.pengaduan.nama);
                          $('#tanggalpengaduandetail').text(tanggal_pengaduan);
                          $('#tgllahirdetail').text(tanggal_lahir);
                          $('#jkdetail').text(data.pengaduan.jk);
                          $('#nohpdetail').text(data.pengaduan.nohp);
                          $('#emaildetail').text(data.pengaduan.email);
                          $('#pekerjaandetail').text(data.pengaduan.pekerjaan);
                          $('#instansidetail').text(data.pengaduan.instansi);
                          $('#uraiandetail').val(data.pengaduan.uraian);
                          $('#tanggaldetail').text(tanggal_kejadian);
                          var bukti = data.bukti;
                          var tableContent = '';

                          for (var i = 0; i < bukti.length; i++) {
                              var row = '<tr>' +
                                  '<td>' + (i + 1) + '</td>' +
                                  '<td>' + bukti[i].nama_file + '</td>' +
                                  '<td><button class="btn btn-primary lihat_file" data-id="' +
                                  bukti[i].path +
                                  '"><i class="bi-eye"></i></button></td>' +
                                  '<td><button class="btn btn-success unduh_file" data-id="' +
                                  bukti[i].path +
                                  '"><i class="bi-download"></i></button></td>' +
                                  '</tr>';
                              tableContent += row;
                          }

                          $('#tableBukti').html(tableContent);
                      },
                      error: function(xhr, status, error) {
                          console.error(xhr.responseText);
                      }
                  });
              });
              $(document).on('click', '.lihat_file', function() {
                  var filePath = $(this).data('id');
                  var fileExtension = filePath.split('.').pop().toLowerCase();
                  var fileContent = '';

                  if (fileExtension === 'jpg' || fileExtension === 'jpeg' || fileExtension === 'png' ||
                      fileExtension === 'gif') {
                      fileContent = '<img src="' + '{{ asset('') }}' + filePath +
                          '" class="img-fluid" alt="Preview">';
                  } else if (fileExtension === 'pdf') {
                      fileContent = '<iframe src="' + '{{ asset('') }}' + filePath +
                          '" width="100%" height="500px"></iframe>';
                  } else if (fileExtension === 'docx') {
                      // Menggunakan Google Docs Viewer untuk menampilkan file DOCX
                      fileContent = '<iframe src="https://view.officeapps.live.com/op/embed.aspx?src=' +
                          encodeURIComponent('{{ asset('') }}' + filePath) +
                          '" width="100%" height="500px"></iframe>';
                  } else if (fileExtension === 'mp4' || fileExtension === 'webm' || fileExtension ===
                      'ogg') {
                      fileContent = '<video width="100%" height="auto" controls><source src="' +
                          '{{ asset('') }}' + filePath + '" type="video/' + fileExtension +
                          '"></video>';
                  } else {
                      fileContent = '<p>File tidak dapat ditampilkan.</p>';
                  }

                  $('#fileContent').html(fileContent);
                  $('#fileModal').modal('show');
              });

              $(document).on('click', '.unduh_file', function() {
                  var filePath = $(this).data('id');
                  var url = '{{ asset('') }}' + filePath;
                  window.open(url, '_blank'); // Membuka file unduhan di tab baru
              });




              $('#reload').click(function() {

                  reloadTable('/list_pengaduan_masyarakat');
              });



              $('#tambah').click(function() {
                  $('#modaltambah').modal('show');
              });

              $('#formverif').submit(function(event) {
                  event.preventDefault(); // Mencegah aksi default form
                  // Lakukan sesuatu di sini, misalnya validasi atau pengiriman data dengan AJAX
              });
              $('#formdispo').submit(function(event) {
                  event.preventDefault(); // Mencegah aksi default form
                  // Lakukan sesuatu di sini, misalnya validasi atau pengiriman data dengan AJAX
              });
              $('#formbag').submit(function(event) {
                  event.preventDefault(); // Mencegah aksi default form
                  // Lakukan sesuatu di sini, misalnya validasi atau pengiriman data dengan AJAX
              });
              $('#formselesai').submit(function(event) {
                  event.preventDefault(); // Mencegah aksi default form
                  // Lakukan sesuatu di sini, misalnya validasi atau pengiriman data dengan AJAX
              });
              $('#formpencarian').submit(function(event) {
                  event.preventDefault(); // Mencegah aksi default form
                  // Lakukan sesuatu di sini, misalnya validasi atau pengiriman data dengan AJAX
              });

          });
      </script>
      <script>
          // Ambil elemen yang dibutuhkan
          const droppableArea = document.getElementById('droppableArea');
          const fileInput = document.getElementById('fileInput');
          const fileList = document.getElementById('fileList');
          const uploadButton = document.getElementById('uploadButton');
          const fileTable = document.getElementById('fileTable');

          // Fungsi untuk menambahkan file ke daftar
          function addFileToList(file) {
              const fileItem = document.createElement('tr');

              const fileNameCell = document.createElement('td');
              fileNameCell.textContent = file.name;

              const fileActionCell = document.createElement('td');
              const fileDeleteButton = document.createElement('button');
              fileDeleteButton.textContent = 'Hapus';
              fileDeleteButton.classList.add('btn', 'btn-sm', 'btn-danger');
              fileDeleteButton.addEventListener('click', () => {
                  fileItem.remove();
                  if (fileList.children.length === 0) {
                      fileTable.style.display = 'none';
                  }
              });

              fileActionCell.appendChild(fileDeleteButton);

              fileItem.appendChild(fileNameCell);
              fileItem.appendChild(fileActionCell);

              fileList.appendChild(fileItem);
              fileTable.style.display = 'block';
          }

          // Tambahkan event listener untuk drag & drop
          droppableArea.addEventListener('dragover', (e) => {
              e.preventDefault();
              droppableArea.classList.add('active');
          });

          droppableArea.addEventListener('dragleave', () => {
              droppableArea.classList.remove('active');
          });

          droppableArea.addEventListener('drop', (e) => {
              e.preventDefault();
              droppableArea.classList.remove('active');

              const files = e.dataTransfer.files;
              Array.from(files).forEach(file => addFileToList(file));
          });

          // Tambahkan event listener untuk klik pada area droppable
          droppableArea.addEventListener('click', () => {
              fileInput.click();
          });

          // Tambahkan event listener untuk memilih file dari input
          fileInput.addEventListener('change', () => {
              const files = fileInput.files;
              Array.from(files).forEach(file => addFileToList(file));
          });

          // Tambahkan event listener untuk tombol Upload
      </script>
  @endsection
