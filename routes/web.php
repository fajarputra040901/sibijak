<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/status_track/{id}', [App\Http\Controllers\AdminController::class, 'status_track']);
Route::get('/refleksi', [App\Http\Controllers\AppController::class, 'refleksi'])->name('Refleksi');
Route::get('/pengaduan', [App\Http\Controllers\AppController::class, 'pengaduan'])->name('Pengaduan Masyarakat');
Route::get('/cek_status', [App\Http\Controllers\AppController::class, 'cek_status']);
Route::get('/hasil-pengaduan/{id}', [App\Http\Controllers\AppController::class, 'kirim_pengaduan'])->name('Pengaduan Masyarakat');
Route::get('/hasil-pengaduan-wbs/{id}', [App\Http\Controllers\AppController::class, 'kirim_pengaduan'])->name('Whistle Blowing System');
Route::post('/pengaduan_store', [App\Http\Controllers\AppController::class, 'pengaduan_store'])->name('pengaduan_store');



Route::get('/wbs', [App\Http\Controllers\AppController::class, 'wbs'])->name('Whistle Blowing System');

Route::get('/cek_token_pelatihan', [App\Http\Controllers\AppController::class, 'cek_token_pelatihan']);
Route::post('/tambah_refleksi_temp', [App\Http\Controllers\AppController::class, 'tambah_refleksi_temp']);
Route::post('/tambah_refleksi', [App\Http\Controllers\AppController::class, 'tambah_refleksi']);
Route::post('/hapus_refleksi_temp', [App\Http\Controllers\AppController::class, 'hapus_refleksi_temp']);
Route::get('/pengaduan_refleksi/{id}', [App\Http\Controllers\AppController::class, 'pengaduan_refleksi']);
Route::get('/detail_refleksi_temp/{id}', [App\Http\Controllers\AppController::class, 'detail_refleksi_temp']);

Auth::routes();
Route::middleware(['auth'])->group(
    function () {
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

        Route::get('/pengaduan_masyarakat_controller', [App\Http\Controllers\AdminController::class, 'pengaduan_masyarakat_controller'])->name('Pengaduan Masyarakat');
        Route::get('/list_pengaduan_masyarakat', [App\Http\Controllers\AdminController::class, 'list_pengaduan_masyarakat']);
        Route::get('/pencarian_pengaduan_masyarakat', [App\Http\Controllers\AdminController::class, 'pencarian_pengaduan_masyarakat']);
        Route::get('/jumlah_pengaduan_masyarakat', [App\Http\Controllers\AdminController::class, 'jumlah_pengaduan_masyarakat']);
        Route::get('/status_verifikasi/{id}', [App\Http\Controllers\AdminController::class, 'status_verifikasi']);
        Route::get('/submit_verifikator', [App\Http\Controllers\AdminController::class, 'submit_verifikator']);
        Route::get('/status_disposisi/{id}', [App\Http\Controllers\AdminController::class, 'status_disposisi']);
        Route::get('/submit_penerimaan', [App\Http\Controllers\AdminController::class, 'submit_penerimaan']);
        Route::get('/submit_bagian', [App\Http\Controllers\AdminController::class, 'submit_bagian']);
        Route::get('/status_tindaklanjut/{id}', [App\Http\Controllers\AdminController::class, 'status_tindaklanjut']);
        Route::post('/submittindaklanjutbagian', [App\Http\Controllers\AdminController::class, 'submittindaklanjutbagian']);
        Route::get('/detail_pengaduan/{id}', [App\Http\Controllers\AdminController::class, 'detail_pengaduan']);
        Route::get('/status_selesai/{id}', [App\Http\Controllers\AdminController::class, 'status_selesai']);
        Route::get('/submit_selesai', [App\Http\Controllers\AdminController::class, 'submitselesai']);
        Route::get('/cek/{id}', [App\Http\Controllers\AdminController::class, 'cek']);
        Route::get('/unduh-file-bukti-pengaduan/{path}', [App\Http\Controllers\AdminController::class, 'unduh_file_bukti_pengaduan']);

        Route::get('/wbs_controller', [App\Http\Controllers\AdminController::class, 'wbs_controller'])->name('Whistle Blowing System');
        Route::get('/list_wbs', [App\Http\Controllers\AdminController::class, 'list_wbs']);
        Route::get('/jumlah_wbs', [App\Http\Controllers\AdminController::class, 'jumlah_wbs']);
        Route::get('/pencarian_wbs', [App\Http\Controllers\AdminController::class, 'pencarian_wbs']);

        Route::get('/pelatihan', [App\Http\Controllers\AdminController::class, 'pelatihan'])->name('Pelatihan');;
        Route::get('/list_pelatihan', [App\Http\Controllers\AdminController::class, 'list_pelatihan']);
        Route::post('/tambah_pelatihan', [App\Http\Controllers\AdminController::class, 'tambah_pelatihan']);
        Route::post('/update_pelatihan', [App\Http\Controllers\AdminController::class, 'update_pelatihan']);
        Route::post('/hapus_pelatihan', [App\Http\Controllers\AdminController::class, 'hapus_pelatihan']);
        Route::get('/jumlah_pelatihan', [App\Http\Controllers\AdminController::class, 'jumlah_pelatihan']);
        Route::get('/detail_pelatihan/{id}', [App\Http\Controllers\AdminController::class, 'detail_pelatihan']);


        Route::get('/refleksi_controller', [App\Http\Controllers\AdminController::class, 'refleksi_controller'])->name('Refleksi');
        Route::get('/list_refleksi', [App\Http\Controllers\AdminController::class, 'list_refleksi']);
        Route::get('/jumlah_refleksi', [App\Http\Controllers\AdminController::class, 'jumlah_refleksi']);
        Route::get('/pencarian_refleksi', [App\Http\Controllers\AdminController::class, 'pencarian_refleksi']);
    }
);
